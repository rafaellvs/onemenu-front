import { useState, useEffect, useRef } from 'react'

export const useDialog = () => {
  const ref = useRef(null)
  const [isDialogOpen, setDialogOpen] = useState(false)

  const handleClick = event => {
    !ref.current.contains(event.target) &&
    setDialogOpen(false)
  }

  useEffect(() => {
    isDialogOpen &&
    document.addEventListener('click', handleClick)

    return () => document.removeEventListener('click', handleClick)
  }, [isDialogOpen])

  return {
    ref,
    isDialogOpen,
    setDialogOpen,
  }
}
