import PropTypes from 'prop-types'

const Places = ({ children }) => children

Places.propTypes = {
  children: PropTypes.node,
}

export default Places
