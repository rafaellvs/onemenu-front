import PropTypes from 'prop-types'

const Place = ({ children }) => children

Place.propTypes = {
  children: PropTypes.node,
}

export default Place
