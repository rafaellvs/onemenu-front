import styled from 'styled-components'

import Button from 'app/components/core/Button'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  margin-bottom: ${theme.navbarHeight};
`

export const Submit = styled.div`
  position: fixed;
  text-align: center;
  bottom: 0;
  left: 0;
  width: 100%;
  background: ${theme.colors.white};
  padding: 1rem;
`

export const SubmitButton = styled(Button)`
  max-width: ${theme.width.default};
`
