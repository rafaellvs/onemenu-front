import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import PropTypes from 'prop-types'

import weekdays from 'app/helpers/weekdays'

import { getPlaceData, editPlace } from 'app/helpers/parse-api/places'

import { validateImage } from 'app/helpers/utils'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import InputFile from 'app/components/core/InputFile'
import Textarea from 'app/components/core/Textarea'

import Categories from 'app/components/Places/Categories'
import WeekdayHours from 'app/components/Places/WeekdayHours'
import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import {
  Container,
  Submit,
  SubmitButton,
} from './styled'

const Edit = ({ id }) => {
  const { register, handleSubmit, errors, control } = useForm()

  const [defaultValues, setDefaultValues] = useState(null)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })
  const [isLoading, setIsLoading] = useState(false)
  const [isDataLoading, setIsDataLoading] = useState(true)

  const onSubmit = async data => {
    setApiStatus({ error: false, success: false })
    setIsLoading(true)

    editPlace(id, data)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Restaurante atualizado com sucesso.', error: false })
      })
      .catch(() => setApiStatus({ error: 'Não foi possível alterar o Restaurante.', success: false }))
  }

  useEffect(() => {
    getPlaceData(id).then(res => {
      setDefaultValues(res)
      setIsDataLoading(false)
    })
  }, [])

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <WhiteBox>
          <Text component='h1' padding='0 0 2rem 0'>
            Editar restaurante
          </Text>

          {isDataLoading && <Loading />}
          {
            !isDataLoading &&
              <>
                <InputFile
                  label='Logo'
                  id='logo'
                  name='logo'
                  ref={register({
                    validate: value => value.length && validateImage(value[0]),
                  })}
                  defaultImage={defaultValues && defaultValues.logo}
                />
                {errors.logo && <StatusMessage message={errors.logo.message} variant='error' />}

                <Label htmlFor='name'>Nome do restaurante</Label>
                <Input
                  type='text'
                  id='name'
                  name='name'
                  placeholder='Nome do restaurante'
                  ref={register()}
                  defaultValue={defaultValues && defaultValues.name}
                />

                <Label htmlFor='description'>Descrição</Label>
                <Textarea
                  type='text'
                  id='description'
                  name='description'
                  placeholder='Conte um pouco sobre seu estabelecimento'
                  ref={register()}
                  defaultValue={defaultValues && defaultValues.description}
                />

                <Label htmlFor='categories'>Tipo de Cozinha</Label>
                <Categories
                  id='categories'
                  register={register}
                  control={control}
                  defaultValue={defaultValues && defaultValues.categories}
                />

                <InputFile
                  label='Ambiente 1'
                  id='environment1'
                  name='environment1'
                  ref={register({
                    validate: value => value.length && validateImage(value[0]),
                  })}
                  defaultImage={defaultValues && defaultValues.env1}
                />
                {errors.environment1 && <StatusMessage message={errors.environment1.message} variant='error' />}

                <InputFile
                  label='Ambiente 2'
                  id='environment2'
                  name='environment2'
                  ref={register({
                    validate: value => value.length && validateImage(value[0]),
                  })}
                  defaultImage={defaultValues && defaultValues.env2}
                />
                {errors.environment2 && <StatusMessage message={errors.environment2.message} variant='error' />}
              </>
          }
        </WhiteBox>

        <WhiteBox margin='2.5rem 0 0 0'>
          <Text component='h3' padding='0 0 2rem 0'>
            Endereço
          </Text>

          {isDataLoading && <Loading />}
          {
            !isDataLoading &&
              <>
                <Label htmlFor='address'>Endereço</Label>
                <Input
                  type='text'
                  id='address'
                  name='address'
                  placeholder='Digite o endereço'
                  ref={register()}
                  defaultValue={defaultValues && defaultValues.address}
                />
              </>
          }
        </WhiteBox>

        <WhiteBox margin='2.5rem 0 0 0'>
          <Text component='h3' padding='0 0 2rem 0'>
            Horário de funcionamento
          </Text>

          {isDataLoading && <Loading />}
          {
            !isDataLoading &&
            weekdays.map(weekday =>
              <WeekdayHours
                key={weekday.id}
                weekday={weekday}
                register={register}
                control={control}
                defaultValue={defaultValues && defaultValues.openingHours}
              />
            )
          }
        </WhiteBox>

        <Submit>
          {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
          {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

          {
            isLoading
              ? <Loading />
              : <SubmitButton type='submit'>Salvar dados</SubmitButton>
          }
        </Submit>
      </Form>
    </Container>
  )
}

Edit.propTypes = {
  id: PropTypes.string,
}

export default Edit
