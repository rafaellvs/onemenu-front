import React, { useState } from 'react'
import { navigate } from '@reach/router'
import { useForm } from 'react-hook-form'

import { createPlace } from 'app/helpers/parse-api/places'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Button from 'app/components/core/Button'

import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import { Container } from './styled'

const Step1 = () => {
  const { register, handleSubmit } = useForm()
  const [apiError, setApiError] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const onSubmit = data => {
    setIsLoading(true)
    setApiError(false)

    createPlace(data)
      .then(() => {
        setIsLoading(false)
        navigate('/places/dashboard')
      })
      .catch(() => {
        setIsLoading(false)
        setApiError('Não foi possível criar o restaurante.')
      })
  }

  return (
    <Container>
      <WhiteBox>
        <Text component='h1' padding='0 0 1rem 0'>
          Cadastro de restaurante
        </Text>

        <Text variant='light' padding='0 0 3rem 0'>
          Coloque abaixo o nome do seu estabelecimento.
        </Text>

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Label htmlFor='name'>
            Nome do restaurante
          </Label>
          <Input
            type='text'
            id='name'
            name='name'
            placeholder='Nome do seu restaurante'
            ref={register({ required: true })}
            required
          />
          {apiError && <StatusMessage message={apiError} variant='error' />}

          {
            isLoading
              ? <Loading />
              : <Button type='submit'>Continuar</Button>
          }
        </Form>
      </WhiteBox>
    </Container>
  )
}

export default Step1
