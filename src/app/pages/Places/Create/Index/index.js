import React from 'react'
import { navigate } from '@reach/router'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Button from 'app/components/core/Button'

import { Container } from './styled'

const Index = () => {
  return (
    <Container>
      <WhiteBox>
        <Text component='h3'>
          Você ainda não cadastrou seu restaurante
        </Text>

        <Text variant='light' padding='2rem 0'>
          Clique no botão abaixo e comece agora o cadastro do seu restaurante.
        </Text>

        <Button onClick={() => navigate('create/step1')}>
          Cadastrar restaurante
        </Button>
      </WhiteBox>
    </Container>
  )
}

export default Index
