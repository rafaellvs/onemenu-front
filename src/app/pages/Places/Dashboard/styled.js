import styled from 'styled-components'

import CoreWhiteBox from 'app/components/core/WhiteBox'

export const Container = styled.div``

export const WhiteBox = styled(CoreWhiteBox)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 1.5rem;
`
