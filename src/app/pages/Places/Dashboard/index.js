import React, { useEffect, useState } from 'react'
import { navigate } from '@reach/router'

import { getUserPlaces } from 'app/helpers/parse-api/places'

import Button from 'app/components/core/Button'

import Place from 'app/components/Places/Place'
import Loading from 'app/components/Loading'

import { Container, WhiteBox } from './styled'

const Dashboard = () => {
  const [places, setPlaces] = useState(null)
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    getUserPlaces().then(res => {
      setPlaces(res)
      setIsLoading(false)
    })
  }, [])

  return (
    <Container>
      {
        isLoading &&
          <WhiteBox>
            <Loading />
          </WhiteBox>
      }

      {
        !isLoading &&
        places.map((place, index) =>
          <Place
            key={index}
            place={place}
          />
        )
      }

      <Button onClick={() => navigate('/places/create/step1')}>
        Cadastrar novo restaurante
      </Button>
    </Container>
  )
}

export default Dashboard
