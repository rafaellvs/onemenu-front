import { useEffect } from 'react'
import { navigate } from '@reach/router'
import Parse from 'parse'

const Index = () => {
  const user = Parse.User.current()

  const redirect = async () => {
    const places = await user.relation('places').query().find()

    places.length
      ? navigate('places/dashboard')
      : navigate('places/create')
  }

  useEffect(() => {
    redirect()
  }, [])

  return null
}

export default Index
