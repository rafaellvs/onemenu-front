import styled from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

import CoreBox from 'app/components/core/Box'

export const Container = styled.div``

export const TabContent = styled.div``

export const Categories = styled.div``

export const Box = styled(CoreBox)`
  padding: ${theme.padding.default};

  ${mediaQueryMax('xSmall', `padding: ${theme.padding.small}`)}
`
