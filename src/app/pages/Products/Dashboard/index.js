import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { useDialog } from 'app/hooks/dialog'

import { getPlaceProductCategories } from 'app/helpers/parse-api/products'

import Tabs from 'app/components/core/Tabs'
import Text from 'app/components/core/Text'

import Category from 'app/components/Products/Category'
import AddItem from 'app/components/Products/AddItem'
import CreateCategory from 'app/components/Products/Dialogs/CreateCategory'
import Loading from 'app/components/Loading'

import {
  Container,
  TabContent,
  Categories,
  Box,
} from './styled'

const Dashboard = ({ id, location }) => {
  const { ref, isDialogOpen, setDialogOpen } = useDialog()

  const [selected, setSelected] = useState('Food')
  const [categories, setCategories] = useState({ food: [], drink: [] })
  const [isLoading, setIsLoading] = useState(true)

  const handleClick = category => {
    setSelected(category)
    setDialogOpen(true)
  }

  const renderCategories = categories =>
    !categories.length
      ? <Box><Text variant='error'>Nenhuma categoria foi encontrada.</Text></Box>
      : (
        categories.map((category, index) =>
          <Category
            key={index}
            category={category}
            opened={location.state.categoryId}
          />
        )
      )

  useEffect(() => {
    getPlaceProductCategories(id).then(res => {
      setCategories(res)
      setIsLoading(false)
    })
  }, [isDialogOpen])

  return (
    <Container>
      <Tabs tabs={['Food', 'Drink']}>
        <TabContent id='food'>
          {isLoading && <Loading margin='2rem auto' />}

          {
            !isLoading &&
              <Categories>
                {renderCategories(categories.food)}

                <AddItem
                  item='Categoria'
                  onClick={() => handleClick('Food')}
                />
              </Categories>
          }
        </TabContent>

        <TabContent id='Drink'>
          {isLoading && <Loading margin='2rem auto' />}

          {
            !isLoading &&
              <Categories>
                {renderCategories(categories.drink)}

                <AddItem
                  item='Categoria'
                  onClick={() => handleClick('Drink')}
                />
              </Categories>
          }
        </TabContent>
      </Tabs>

      <CreateCategory
        id={id}
        ref={ref}
        setOpen={setDialogOpen}
        isOpen={isDialogOpen}
        selected={selected}
      />
    </Container>
  )
}

Dashboard.propTypes = {
  id: PropTypes.string,
  location: PropTypes.object,
}

export default Dashboard
