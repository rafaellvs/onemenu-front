import styled from 'styled-components'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  margin-bottom: ${theme.navbarHeight};
`

export const Submit = styled.div`
  position: fixed;
  text-align: center;
  bottom: 0;
  left: 0;
  width: 100%;
  background: ${theme.colors.white};
  padding: 1rem;

  button {
    max-width: ${theme.width.default};
  }
`

export const Radio = styled.div`
  padding: 2rem 0;
`
