import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { navigate } from '@reach/router'
import PropTypes from 'prop-types'

import { createProduct } from 'app/helpers/parse-api/products'

import { validateImage, generateSelectProductCategories } from 'app/helpers/utils'

import Box from 'app/components/core/Box'
import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import InputFile from 'app/components/core/InputFile'
import Textarea from 'app/components/core/Textarea'
import Select from 'app/components/core/Select'
import InputSwitch from 'app/components/core/InputSwitch'
import InputRadio from 'app/components/core/InputRadio'
import Button from 'app/components/core/Button'

import ProductPrice from 'app/components/Products/ProductPrice'
import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import { Container, Submit } from './styled'

const CreateProduct = ({ id, location }) => {
  const { register, handleSubmit, errors, watch, control } = useForm()

  const [categories, setCategories] = useState(null)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })
  const [isLoading, setIsLoading] = useState(false)
  const priceSelector = watch('priceSelector')

  const onSubmit = async data => {
    setApiStatus({ error: false, success: false })
    setIsLoading(true)

    createProduct(id, data)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Prato criado com sucesso.', error: false })
      })
      .catch(() => {
        setIsLoading(false)
        setApiStatus({ error: 'Não foi possível criar o prato.', success: false })
      })
  }

  const handleClickBack = () =>
    navigate('dashboard', { state: { categoryId: location.state.categoryId } })

  useEffect(() => {
    generateSelectProductCategories(id)
      .then(res => setCategories(res))
  }, [])

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <WhiteBox>
          <Text component='h1' padding='0 0 2rem 0'>
            Cadastrar prato
          </Text>

          <InputFile
            label='Imagem destaque do prato'
            id='image'
            name='image'
            ref={register({
              validate: value => value.length && validateImage(value[0]),
            })}
          />
          {errors.image && <StatusMessage message={errors.image.message} variant='error' />}

          <Label htmlFor='name'>Nome do prato</Label>
          <Input
            type='text'
            id='name'
            name='name'
            placeholder='Nome do prato'
            ref={register({ required: true })}
            required
          />

          <Label htmlFor='category'>Categoria do produto</Label>
          <Select
            id='category'
            name='category'
            defaultValue={location.state.categoryId}
            ref={register({ required: true })}
            required
          >
            {categories}
          </Select>

          <Label htmlFor='description'>Descrição</Label>
          <Textarea
            type='text'
            id='description'
            name='description'
            placeholder='Conte um pouco sobre o prato'
            ref={register()}
          />

          <InputSwitch
            id='status'
            name='status'
            label='Status:'
            statusText
            checked
            ref={register()}
          />
        </WhiteBox>

        <WhiteBox margin='2.5rem 0 0 0'>
          <Text component='h3'>
            Preço
          </Text>

          <Box padding='2rem 0 1.5rem 0'>
            <InputRadio register={register}>
              <input
                type='radio'
                id='priceSimple'
                name='priceSelector'
                value='simple'
                label='Preço Simples'
                checked
              />
              <input
                type='radio'
                id='priceCustom'
                name='priceSelector'
                value='custom'
                label='Preço Personalizado'
              />
            </InputRadio>
          </Box>

          <ProductPrice
            priceSelector={priceSelector}
            register={register}
            control={control}
          />
        </WhiteBox>

        <Submit>
          {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
          {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

          {
            isLoading
              ? <Loading />
              : apiStatus.success
                ? <Button type='button' onClick={handleClickBack}>Voltar</Button>
                : <Button type='submit'>Adicionar prato</Button>
          }
        </Submit>
      </Form>
    </Container>
  )
}

CreateProduct.propTypes = {
  id: PropTypes.string,
  location: PropTypes.object,
}

export default CreateProduct
