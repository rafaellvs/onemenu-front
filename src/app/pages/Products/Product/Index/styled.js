import styled from 'styled-components'

import theme from 'app/helpers/theme'

import CoreButton from 'app/components/core/Button'
import Image from 'app/components/core/Image'

export const Container = styled.div`
  margin-bottom: ${theme.navbarHeight};

  p {
    padding: .5rem 0 2rem;

    &:last-child {
      padding-bottom: 0;
    }
  }
`

export const Picture = styled(Image)`
  max-width: 100%;
  border-radius: ${theme.borderRadius.box};
  margin: 2rem 0;
`

export const ButtonsContainer = styled.div`
  position: fixed;
  display: flex;
  justify-content: center;
  text-align: center;
  bottom: 0;
  left: 0;
  width: 100%;
  background: ${theme.colors.white};
  padding: 1rem;
`

export const Buttons = styled.div`
  display: flex;
  justify-content: space-between;
  width: ${theme.width.default};
`

export const Button = styled(CoreButton)`
  width: 45%;
`
