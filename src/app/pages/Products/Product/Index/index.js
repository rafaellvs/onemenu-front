import React, { useState, useEffect } from 'react'
import { navigate } from '@reach/router'
import PropTypes from 'prop-types'

import { useDialog } from 'app/hooks/dialog'

import { getProduct, getProductCategory } from 'app/helpers/parse-api/products'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Box from 'app/components/core/Box'

import Loading from 'app/components/Loading'
import RemoveProduct from 'app/components/Products/Dialogs/RemoveProduct'

import {
  Container,
  Picture,
  ButtonsContainer,
  Buttons,
  Button,
} from './styled'

const Product = ({ id }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [product, setProduct] = useState(null)
  const [category, setCategory] = useState(null)

  const { ref, isDialogOpen, setDialogOpen } = useDialog()

  const handlePictureSrc = () =>
    product.get('image')
      ? product.get('image')._url
      : null

  useEffect(() => {
    getProduct(id)
      .then(res => {
        setProduct(res)
        setIsLoading(false)
      })
      .catch(() => {
        setProduct(null)
        setIsLoading(false)
      })
  }, [isDialogOpen])

  // get product category
  useEffect(() => {
    product &&
    getProductCategory(product.get('category').id)
      .then(res => setCategory(res))
  }, [product])

  return (
    <Container>
      <WhiteBox>
        {isLoading && <Loading />}

        {
          !isLoading &&
          !product &&
            <Box align='center'>
              <Text variant='error'>Produto não encontrado.</Text>
              <Button onClick={() => navigate('dashboard')}>
                Voltar
              </Button>
            </Box>
        }

        {
          !isLoading &&
          product &&
            <>
              <Text component='h3'>
                {product.get('name')}
              </Text>
              <Picture src={handlePictureSrc()} />

              <Text component='h4' variant='light'>
                Preço
              </Text>
              <Text>
                {
                  product.get('price')
                    ? `R$ ${product.get('price').toFixed(2).replace('.', ',')}`
                    : '--'
                }
              </Text>

              <Text component='h4' variant='light'>
                Categoria do prato
              </Text>
              {category && <Text>{category.get('name')}</Text>}

              <Text component='h4' variant='light'>
                Descrição
              </Text>
              <Text>
                {product.get('description')}
              </Text>

              <Text component='h4' variant='light'>
                Status
              </Text>
              <Text>
                {product.get('status') ? 'Ativo' : 'Inativo'}
              </Text>
            </>
        }
      </WhiteBox>

      {
        product &&
          <>
            <ButtonsContainer>
              <Buttons>
                <Button remove onClick={() => setDialogOpen(true)}>
                  Excluir
                </Button>

                <Button onClick={() => navigate(`${product.id}/edit`)}>
                  Editar
                </Button>
              </Buttons>
            </ButtonsContainer>

            <RemoveProduct
              id={id}
              name={product.get('name')}
              ref={ref}
              isOpen={isDialogOpen}
              setOpen={setDialogOpen}
            />
          </>
      }
    </Container>
  )
}

Product.propTypes = {
  id: PropTypes.string,
}

export default Product
