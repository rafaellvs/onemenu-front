import PropTypes from 'prop-types'

const Product = ({ children }) => children

Product.propTypes = {
  children: PropTypes.node,
}

export default Product
