import React, { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useNavigate } from '@reach/router'
import PropTypes from 'prop-types'

import { getProductData, editProduct } from 'app/helpers/parse-api/products'

import { validateImage, generateSelectProductCategoriesByProductId } from 'app/helpers/utils'

import Box from 'app/components/core/Box'
import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import InputFile from 'app/components/core/InputFile'
import Textarea from 'app/components/core/Textarea'
import Select from 'app/components/core/Select'
import InputSwitch from 'app/components/core/InputSwitch'
import InputRadio from 'app/components/core/InputRadio'
import Button from 'app/components/core/Button'

import ProductPrice from 'app/components/Products/ProductPrice'
import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import { Container, Submit } from './styled'

const EditProduct = ({ id }) => {
  const { register, handleSubmit, errors, watch, control } = useForm()

  const [defaultValues, setDefaultValues] = useState(null)
  const [categories, setCategories] = useState(null)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })
  const [isLoading, setIsLoading] = useState(false)
  const [isDataLoading, setIsDataLoading] = useState(true)

  const priceSelector = watch('priceSelector')
  const navigate = useNavigate()

  const onSubmit = async data => {
    setApiStatus({ error: false, success: false })
    setIsLoading(true)

    editProduct(id, data)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Prato atualizado com sucesso.', error: false })
      })
      .catch(() => setApiStatus({ error: 'Não foi possível alterar o Prato.', success: false }))
  }

  useEffect(() => {
    getProductData(id).then(res => {
      setDefaultValues(res)
      setIsDataLoading(false)
    })

    generateSelectProductCategoriesByProductId(id)
      .then(res => setCategories(res))
  }, [])

  return (
    <Container>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <WhiteBox>
          <Text component='h1' padding='0 0 2rem 0'>
            Editar prato
          </Text>

          {isDataLoading && <Loading />}

          {
            !isDataLoading &&
              <>
                <InputFile
                  label='Imagem destaque do prato'
                  id='image'
                  name='image'
                  ref={register({
                    validate: value => value.length && validateImage(value[0]),
                  })}
                  defaultImage={defaultValues && defaultValues.image}
                />
                {errors.image && <StatusMessage message={errors.image.message} variant='error' />}

                <Label htmlFor='name'>Nome do prato</Label>
                <Input
                  type='text'
                  id='name'
                  name='name'
                  placeholder='Nome do prato'
                  ref={register({ required: true })}
                  required
                  defaultValue={defaultValues && defaultValues.name}
                />

                <Label htmlFor='category'>Categoria do produto</Label>
                <Select
                  id='category'
                  name='category'
                  ref={register({ required: true })}
                  required
                  defaultValue={defaultValues && defaultValues.category}
                >
                  {categories}
                </Select>

                <Label htmlFor='description'>Descrição</Label>
                <Textarea
                  type='text'
                  id='description'
                  name='description'
                  placeholder='Conte um pouco sobre o prato'
                  ref={register()}
                  defaultValue={defaultValues && defaultValues.description}
                />

                <InputSwitch
                  id='status'
                  name='status'
                  label='Status:'
                  statusText
                  ref={register()}
                  checked={defaultValues && defaultValues.status}
                />
              </>
          }
        </WhiteBox>

        <WhiteBox margin='2.5rem 0 0 0'>
          <Text component='h3'>
            Preço
          </Text>

          {isDataLoading && <Loading />}

          {
            !isDataLoading &&
              <>
                <Box padding='2rem 0 1.5rem 0'>
                  <InputRadio register={register}>
                    <input
                      type='radio'
                      id='priceSimple'
                      name='priceSelector'
                      value='simple'
                      label='Preço Simples'
                      checked
                    />
                    <input
                      type='radio'
                      id='priceCustom'
                      name='priceSelector'
                      value='custom'
                      label='Preço Personalizado'
                    />
                  </InputRadio>
                </Box>

                <ProductPrice
                  priceSelector={priceSelector}
                  register={register}
                  control={control}
                  defaultValue={defaultValues && defaultValues.price}
                />
              </>
          }
        </WhiteBox>

        <Submit>
          {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
          {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

          {
            isLoading
              ? <Loading />
              : apiStatus.success
                ? <Button type='button' onClick={() => navigate('../dashboard')}>Voltar</Button>
                : <Button type='submit'>Editar prato</Button>
          }
        </Submit>
      </Form>
    </Container>
  )
}

EditProduct.propTypes = {
  id: PropTypes.string,
}

export default EditProduct
