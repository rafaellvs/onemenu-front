import PropTypes from 'prop-types'

const Products = ({ children }) => children

Products.propTypes = {
  children: PropTypes.node,
}

export default Products
