import React, { useState } from 'react'
import { navigate } from '@reach/router'
import { useForm } from 'react-hook-form'

import { signup } from 'app/helpers/parse-api/account'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Button from 'app/components/core/Button'

import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import { Container } from './styled'

const SignUp = () => {
  const { register, handleSubmit, errors, watch } = useForm()
  const [apiError, setApiError] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const onSubmit = data => {
    setApiError(false)
    setIsLoading(true)

    signup(data)
      .then(() => {
        setIsLoading(false)
        navigate('places')
      })
      .catch(() => {
        setIsLoading(false)
        setApiError('Este e-mail já está cadastrado.')
      })
  }

  return (
    <Container>
      <WhiteBox>
        <Text component='h1'>
          Crie sua conta
        </Text>
        <Text variant='light' padding='1rem 0 3rem 0'>
          Crie sua conta para iniciar o cadastro do seu restaurante.
        </Text>

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Label htmlFor='name'>Nome</Label>
          <Input
            type='text'
            id='name'
            name='name'
            placeholder='Seu nome completo'
            ref={register({ required: true })}
            required
          />

          <Label htmlFor='email'>E-mail</Label>
          <Input
            type='email'
            id='email'
            name='email'
            placeholder='Seu e-mail'
            ref={register({ required: true })}
            required
          />
          {apiError && <StatusMessage message={apiError} variant='error' />}

          <Label htmlFor='email-match'>Confirmação de e-mail</Label>
          <Input
            type='email'
            id='email-match'
            name='email-match'
            placeholder='Seu e-mail'
            ref={register({
              required: true,
              validate: value => value === watch('email') || 'Os e-mails devem ser iguais.',
            })}
            required
          />
          {errors['email-match'] && <StatusMessage message={errors['email-match'].message} variant='error' />}

          <Label htmlFor='password'>Senha</Label>
          <Input
            type='password'
            id='password'
            name='password'
            placeholder='Sua senha'
            ref={register({ required: true })}
            required
          />

          <Label htmlFor='password-match'>Confirmação de senha</Label>
          <Input
            type='password'
            id='password-match'
            name='password-match'
            placeholder='Sua senha'
            ref={register({
              required: true,
              validate: value => value === watch('password') || 'As senhas devem ser iguais.',
            })}
            required
          />
          {errors['password-match'] && <StatusMessage message={errors['password-match'].message} variant='error' />}

          {
            isLoading
              ? <Loading />
              : <Button type='submit'>Continuar</Button>
          }
        </Form>
      </WhiteBox>
    </Container>
  )
}

export default SignUp
