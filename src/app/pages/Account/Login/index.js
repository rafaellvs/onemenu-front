import React, { useState } from 'react'
import { navigate } from '@reach/router'
import { useForm } from 'react-hook-form'

import logoVertical from 'assets/images/logo-vertical.svg'

import { login } from 'app/helpers/parse-api/account'

import WhiteBox from 'app/components/core/WhiteBox'
import Image from 'app/components/core/Image'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Link from 'app/components/core/Link'
import Button from 'app/components/core/Button'

import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

import { Container } from './styled'

const Login = () => {
  const { register, handleSubmit } = useForm()
  const [apiError, setApiError] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const onSubmit = data => {
    setApiError(false)
    setIsLoading(true)
    login(data)
      .then(() => {
        setIsLoading(false)
        navigate('places')
      })
      .catch(() => {
        setIsLoading(false)
        setApiError('E-mail ou senha inválidos.')
      })
  }

  return (
    <Container>
      <WhiteBox>
        <Image src={logoVertical} width='200px' />

        <Text variant='light' padding='2rem 0'>
          O app que conecta você ao seu cliente de maneira fácil, rápida e segura
        </Text>

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Label htmlFor='email'>E-mail</Label>
          <Input
            type='email'
            id='email'
            name='email'
            placeholder='Seu e-mail'
            ref={register({ required: true })}
            required
          />

          <Label htmlFor='password'>Senha</Label>
          <Input
            type='password'
            id='password'
            name='password'
            placeholder='Sua senha'
            ref={register({ required: true })}
            required
          />
          {apiError && <StatusMessage message={apiError} variant='error' />}

          <Link href='resetpassword' variant='light'>
            Esqueci meu e-mail ou senha
          </Link>

          {
            isLoading
              ? <Loading />
              : <Button type='submit' margin='2rem 0'>Entrar</Button>
          }
        </Form>

        <Link href='signup'>
          Cadastre seu restaurante agora.
        </Link>
      </WhiteBox>
    </Container>
  )
}

export default Login
