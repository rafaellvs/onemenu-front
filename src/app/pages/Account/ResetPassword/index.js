import React from 'react'
import { navigate } from '@reach/router'

import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Button from 'app/components/core/Button'

import { Container } from './styled'

const ResetPassword = () => {
  return (
    <Container>
      <WhiteBox>
        <Text component='h1' padding='0 0 2rem 0'>
          Alterar senha
        </Text>

        <Label htmlFor='password'>Nova senha</Label>
        <Input
          type='password'
          id='password'
          name='password'
          placeholder='Sua nova senha'
        />

        <Label htmlFor='password-match'>Confirmação de senha</Label>
        <Input
          type='password'
          id='password-match'
          name='password-match'
          placeholder='Sua senha'
        />

        <Button onClick={() => navigate('restaurant')}>
          Continuar
        </Button>
      </WhiteBox>
    </Container>
  )
}

export default ResetPassword
