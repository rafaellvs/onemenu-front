import React from 'react'
import ReactDOM from 'react-dom'
import { LocationProvider } from '@reach/router'
import Parse from 'parse'
import WebFont from 'webfontloader'

import Router from 'app/components/Router'
import GlobalStyles from 'app/helpers/global-styles'

WebFont.load({
  google: {
    families: [
      'Roboto: 300,400,500,600,700,800',
    ],
  },
})

Parse.initialize(
  'Q9Vrt6JEVjLoeB3nRVBZmpKrmFQ0zE6FporOLDdm',
  'tH07B90DiKKUE23XxR6RLPwoWZGZphG7s6EOTsjQ'
)
Parse.serverURL = 'https://parseapi.back4app.com/'

const App = () => (
  <LocationProvider>
    <GlobalStyles />
    <Router />
  </LocationProvider>
)

ReactDOM.render(<App />, document.getElementById('root'))
