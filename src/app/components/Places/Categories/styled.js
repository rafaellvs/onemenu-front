import styled from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 1rem;

  select {
    width: 90%;
    margin-right: 1rem;
    margin-bottom: 0;

    ${mediaQueryMax('xSmall', 'width: 77%')}
  }
`

export const IconButton = styled.button`
  border: none;
  background: none;
  padding-right: .5rem;
  transition: color .3s;

  &:hover:not([disabled]) {
    svg {
    color: ${theme.colors.primary};
    cursor: pointer;
    }
  }

  &:disabled {
    svg {
      color: ${theme.colors.gray};
    }
  }
`
