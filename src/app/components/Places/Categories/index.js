import React, { useState, useEffect } from 'react'
import { useFieldArray } from 'react-hook-form'
import PropTypes from 'prop-types'

import { faPlusSquare, faMinusSquare } from '@fortawesome/free-solid-svg-icons'

import { generateSelectCategories } from 'app/helpers/utils'

import Select from 'app/components/core/Select'
import Icon from 'app/components/core/Icon'

import { Container, IconButton } from './styled'

const Categories = ({ register, control, defaultValue, ...restProps }) => {
  const [categories, setCategories] = useState(null)

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'categories',
  })

  useEffect(() => {
    generateSelectCategories()
      .then(res => setCategories(res))
  }, [])

  useEffect(() => {
    defaultValue.map((value, index) => {
      fields.push({ id: index, category: value })
    })

    !defaultValue.length && append({})
  }, [defaultValue])

  return (
    fields.map((item, index) =>
      <Container key={item.id}>
        <Select
          name={`categories[${index}].id`}
          ref={register()}
          {...restProps}
          defaultValue={item.category}
        >
          {categories}
        </Select>

        {
          index === fields.length - 1 &&
            <>
              <IconButton
                type='button'
                onClick={() => remove(index)}
                disabled={index === 0}
              >
                <Icon icon={faMinusSquare} />
              </IconButton>

              <IconButton
                type='button'
                onClick={() => append({})}
              >
                <Icon icon={faPlusSquare} />
              </IconButton>
            </>
        }
      </Container>
    )
  )
}

Categories.propTypes = {
  register: PropTypes.func,
  control: PropTypes.object,
  defaultValue: PropTypes.array,
}

export default Categories
