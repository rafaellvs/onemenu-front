import React from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'
import RouterLink from 'app/components/core/RouterLink'

import { Container } from './styled'

const CompleteRegistration = ({ id }) =>
  <Container>
    <Text component='h3' padding='0 0 1rem 0'>
      Complete seu cadastro
    </Text>

    <Text variant='light'>
      Para que seu restaurante fique ativo e seus clientes possam acessar o menu, é preciso que você complete o cadastro do estabelecimento.&nbsp;
      <RouterLink to={`../${id}/edit`}>Clique aqui e complete agora.</RouterLink>
    </Text>
  </Container>

CompleteRegistration.propTypes = {
  id: PropTypes.string,
}

export default CompleteRegistration
