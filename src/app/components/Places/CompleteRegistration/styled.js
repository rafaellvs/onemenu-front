import styled from 'styled-components'

import theme from 'app/helpers/theme'

import WhiteBox from 'app/components/core/WhiteBox'

export const Container = styled(WhiteBox)`
  border-radius: unset;
  border-left: 4px solid ${theme.colors.warning};
  margin-bottom: 1.5rem;
`
