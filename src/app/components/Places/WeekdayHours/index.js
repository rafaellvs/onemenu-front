import React, { useEffect, useState } from 'react'
import { useFieldArray } from 'react-hook-form'
import PropTypes from 'prop-types'

import { faPlusSquare, faMinusSquare } from '@fortawesome/free-solid-svg-icons'

import { handleTimeInput } from 'app/helpers/utils'

import Icon from 'app/components/core/Icon'

import {
  Weekday,
  CheckboxInput,
  CheckboxLabel,
  Hours,
  HoursInput,
  IconButton,
} from './styled'

const WeekdayHours = ({ weekday, register, control, defaultValue }) => {
  const [isChecked, setIsChecked] = useState(false)

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'hours',
  })

  useEffect(() => {
    defaultValue.map((schedule, index) => {
      if (schedule.weekDay === weekday.id) {
        setIsChecked(true)
        fields.push({
          id: index,
          timeStart: schedule.timeStart,
          timeEnd: schedule.timeEnd,
        })
      }
    })
  }, [defaultValue])

  // appends if day is checked
  useEffect(() => {
    isChecked &&
    fields.length === 0 &&
    append({})
  }, [isChecked])

  return (
    <Weekday>
      <CheckboxInput
        type='checkbox'
        id={weekday.label}
        name={`${weekday.label}.isOpen`}
        ref={register()}
        checked={isChecked}
        onChange={() => setIsChecked(!isChecked)}
      />
      <CheckboxLabel htmlFor={weekday.label}>
        {weekday.name}
      </CheckboxLabel>

      {
        isChecked &&
        fields.map((item, index) =>
          <Hours key={item.id}>
            <HoursInput
              type='text'
              name={`${weekday.label}.hours[${index}].timeStart`}
              placeholder='Abre (hh:mm)'
              maxLength='5'
              ref={register()}
              onKeyDown={event => handleTimeInput(event)}
              defaultValue={item.timeStart}
            />
            <HoursInput
              type='text'
              name={`${weekday.label}.hours[${index}].timeEnd`}
              placeholder='Fecha (hh:mm)'
              maxLength='5'
              ref={register()}
              onKeyDown={event => handleTimeInput(event)}
              defaultValue={item.timeEnd}
            />

            {
              index === fields.length - 1 &&
                <>
                  <IconButton
                    type='button'
                    onClick={() => remove(index)}
                    disabled={index === 0}
                  >
                    <Icon icon={faMinusSquare} />
                  </IconButton>

                  <IconButton
                    type='button'
                    onClick={() => append()}
                  >
                    <Icon icon={faPlusSquare} />
                  </IconButton>
                </>
            }
          </Hours>
        )
      }
    </Weekday>
  )
}

WeekdayHours.propTypes = {
  weekday: PropTypes.object,
  register: PropTypes.func,
  control: PropTypes.object,
  defaultValue: PropTypes.array,
}

export default WeekdayHours
