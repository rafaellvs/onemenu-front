import styled from 'styled-components'

import theme from 'app/helpers/theme'

import Input from 'app/components/core/Input'
import Label from 'app/components/core/Label'

export const Weekday = styled.div`
  padding-bottom: 1rem;
`

export const CheckboxInput = styled(Input)`
  display: inline-block;
  width: unset;
  margin-right: 1rem;
  margin-bottom: 0;
`

export const CheckboxLabel = styled(Label)`
  display: inline-block;
`

export const Hours = styled.div`
  display: flex;
  align-items: center;
  padding: .3rem 0;
`

export const HoursInput = styled(Input)`
  display: inline-block;
  width: 38%;
  padding: .7rem 1rem;
  margin: 0;
  margin-right: .5rem;
`

export const IconButton = styled.button`
  border: none;
  background: none;
  padding-right: .5rem;
  transition: color .3s;

  &:hover:not([disabled]) {
    svg {
    color: ${theme.colors.primary};
    cursor: pointer;
    }
  }

  &:disabled {
    svg {
      color: ${theme.colors.gray};
    }
  }
`
