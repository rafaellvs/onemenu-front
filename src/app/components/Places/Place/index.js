import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import {
  faQrcode,
  faBookOpen,
  faPencilAlt,
  faCopy,
} from '@fortawesome/free-solid-svg-icons'

import { isPlaceFullyRegistered } from 'app/helpers/utils'

import Text from 'app/components/core/Text'
import Link from 'app/components/core/Link'
import RouterLink from 'app/components/core/RouterLink'
import IconButton from 'app/components/core/IconButton'

import CompleteRegistration from 'app/components/Places/CompleteRegistration'

import {
  WhiteBox,
  Logo,
  LogoPlaceholder,
  Icons,
} from './styled'

const Place = ({ place }) => {
  const [isRegistered, setIsRegistered] = useState(true)

  const renderLogo = place =>
    place.get('avatar')
      ? <Logo src={place.get('avatar')._url} />
      : <LogoPlaceholder>Logo</LogoPlaceholder>

  const copyUrl = route => {
    navigator.clipboard.writeText(`https://web.onemenu.com.br/${route}`)
  }

  useEffect(() => {
    isPlaceFullyRegistered(place)
      .then(res => setIsRegistered(res))
  }, [])

  return (
    <>
      {!isRegistered && <CompleteRegistration id={place.id} />}

      <WhiteBox>
        {renderLogo(place)}

        <Text padding='2rem 0'>
          {place.get('name')}
        </Text>

        <Icons>
          <Link href={place.get('pdf')._url} target='_blank'>
            <IconButton icon={faQrcode}>
              Baixar QR Code
            </IconButton>
          </Link>

          <IconButton icon={faCopy} onClick={copyUrl(place.get('route'))}>
            Copiar endereço
          </IconButton>

          <RouterLink to={`/places/${place.id}/products/dashboard`}>
            <IconButton icon={faBookOpen}>
              Editar cardápio
            </IconButton>
          </RouterLink>

          <RouterLink to={`/places/${place.id}/edit`}>
            <IconButton icon={faPencilAlt}>
              Editar restaurante
            </IconButton>
          </RouterLink>
        </Icons>
      </WhiteBox>
    </>
  )
}

Place.propTypes = {
  place: PropTypes.object,
}

export default Place
