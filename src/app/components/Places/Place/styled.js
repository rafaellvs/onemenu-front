import styled from 'styled-components'

import CoreWhiteBox from 'app/components/core/WhiteBox'
import Image from 'app/components/core/Image'

import theme from 'app/helpers/theme'

export const WhiteBox = styled(CoreWhiteBox)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 1.5rem;
`

export const Logo = styled(Image)`
  width: 100px;
  height: 100px;
  border-radius: 50%;
`

export const LogoPlaceholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${theme.colors.white};
  font-weight: bold;
  background: ${theme.colors.gray};
  width: 100px;
  height: 100px;
  border-radius: 50%;
`

export const Icons = styled.div`
  display: flex;
  flex-direction: column;
  
  button {
    margin: .3rem 0;
  }

  a {
    text-decoration: none;
  }
`
