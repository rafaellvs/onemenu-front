import React from 'react'
import { navigate } from '@reach/router'
import PropTypes from 'prop-types'

import { faHouseUser, faLock } from '@fortawesome/free-solid-svg-icons'

import { logout } from 'app/helpers/parse-api/account'

import theme from 'app/helpers/theme'

import Icon from 'app/components/core/Icon'
import Text from 'app/components/core/Text'
import Link from 'app/components/core/Link'

import {
  Container,
  List,
  Item,
  IconContainer,
} from './styled'

const Menu = ({ isMenuOpen, setMenuOpen }) => {
  const handleLogout = () =>
    logout().then(() => {
      setMenuOpen(false)
      navigate('/')
    })

  return (
    <Container isMenuOpen={isMenuOpen}>
      <List>
        <Item href='/restaurant/dashboard'>
          <IconContainer>
            <Icon icon={faHouseUser} color={theme.colors.gray} />
          </IconContainer>
          <Text variant='light'>
            Restaurantes
          </Text>
        </Item>

        <Item href='/resetpassword'>
          <IconContainer>
            <Icon icon={faLock} color={theme.colors.gray} />
          </IconContainer>
          <Text variant='light'>
            Alterar Senha
          </Text>
        </Item>

        <Link
          onClick={handleLogout}
          variant='error'
          textDecoration='none'
        >
          Sair
        </Link>
      </List>
    </Container>
  )
}

Menu.propTypes = {
  isMenuOpen: PropTypes.bool,
  setMenuOpen: PropTypes.func,
}

export default Menu
