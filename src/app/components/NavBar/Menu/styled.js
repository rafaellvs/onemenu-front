import styled from 'styled-components'

import theme from 'app/helpers/theme'

import CoreList from 'app/components/core/List'
import Link from 'app/components/core/Link'

export const Container = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  height: calc(100vh - ${theme.navbarHeight});
  min-width: 360px;
  top: ${theme.navbarHeight};
  right: ${({ isMenuOpen }) => isMenuOpen ? 0 : '-360px'};
  padding: 2rem;
  background: ${theme.colors.white};
  border-left: 70px solid ${theme.colors.gray};
  transition: right .3s;
`

export const List = styled(CoreList)`
  display: flex;
  flex-direction: column;
  height: 100%;

  li:last-child {
    flex: 1;
    display: flex;
    align-items: flex-end;
  }
`

export const Item = styled(Link)`
  display: flex;
  align-items: center;
  padding: .5rem 0;
  text-decoration: none;

  &:hover {
    p, svg {
      color: ${theme.colors.primary};
    }
  }
`

export const IconContainer = styled.div`
  width: 30px;
`
