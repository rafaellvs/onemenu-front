import React, { useState } from 'react'

import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

import { getCurrentUser } from 'app/helpers/parse-api/account'

import onemenuLogo from 'assets/images/icon.png'

import theme from 'app/helpers/theme'

import Link from 'app/components/core/Link'
import Image from 'app/components/core/Image'
import Icon from 'app/components/core/Icon'

import Menu from './Menu'

import {
  Container,
  LogoContainer,
  MenuButton,
} from './styled'

const NavBar = () => {
  const [isMenuOpen, setMenuOpen] = useState(false)

  const handleClick = () =>
    setMenuOpen(!isMenuOpen)

  return (
    <Container id='navbar'>
      <LogoContainer>
        <Link href='/'>
          <Image src={onemenuLogo} width='50px' />
        </Link>
      </LogoContainer>

      {
        getCurrentUser() &&
          <MenuButton onClick={handleClick}>
            {
              isMenuOpen
                ? <Icon icon={faTimes} fontSize='1.2rem' color={theme.colors.white} />
                : <Icon icon={faBars} fontSize='1.2rem' color={theme.colors.white} />
            }
          </MenuButton>
      }

      <Menu
        isMenuOpen={isMenuOpen}
        setMenuOpen={setMenuOpen}
      />
    </Container>
  )
}

export default NavBar
