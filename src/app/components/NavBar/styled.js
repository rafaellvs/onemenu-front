import styled from 'styled-components'

import theme from 'app/helpers/theme'

export const Container = styled.nav`
  position: fixed;
  z-index: 100;
  justify-content: space-between;
  width: 100%;
  display: flex;
  align-items: center;
  background: ${theme.colors.secondary};
  height: ${theme.navbarHeight};
`

export const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${theme.colors.primary};
  width: ${theme.navbarHeight};
  height: 100%;
`

export const MenuButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${theme.navbarHeight};
  height: 100%;

  &:hover {
    cursor: pointer;

    & svg {
      color: ${theme.colors.primary};
    }
  }
`
