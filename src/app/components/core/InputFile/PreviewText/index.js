import React from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'

const PreviewText = ({ file }) =>
  <div>
    <Text variant='light'>
      {file.name}
    </Text>
    <Text variant='light'>
      {(file.size / 1000000).toFixed(2)}MB
    </Text>
  </div>

PreviewText.propTypes = {
  file: PropTypes.object,
}

export default PreviewText
