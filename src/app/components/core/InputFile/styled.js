import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

export const Label = styled.label`
  ${css`${theme.variants.default}`}
  ${css`${theme.variants.light}`}

  display: block;
  width: ${({ width }) => width || '100%'};
  min-width: ${({ minWidth }) => minWidth};
  text-align: center;
  padding: 1rem;
  border: dashed 2px ${theme.colors.lightGray};
  border-radius: ${theme.borderRadius.default};
  margin-bottom: 1.2rem;
  cursor: pointer;
  transition: color .3s, border-color .3s;

  &:hover {
    color: ${theme.colors.primary};
    border-color: ${theme.colors.primary};
  }
`

export const Input = styled.input`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
`

export const Preview = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
