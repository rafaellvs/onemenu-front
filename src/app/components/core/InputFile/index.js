import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'
import Image from 'app/components/core/Image'

import PreviewText from './PreviewText'

import {
  Label,
  Input,
  Preview,
} from './styled'

const InputFile = React.forwardRef(({ id, label, defaultImage, ...restProps }, ref) => {
  const [file, setFile] = useState(null)
  const [imagePreview, setImagePreview] = useState(null)

  const handleChange = event => {
    if (event.target.files[0]) {
      setFile(event.target.files[0])

      const reader = new FileReader()
      reader.readAsDataURL(event.target.files[0])
      reader.onload = () => {
        /data:image\/*/.test(reader.result)
          ? setImagePreview(reader.result)
          : setImagePreview(null)
      }
    }
  }

  useEffect(() => {
    defaultImage && setImagePreview(defaultImage)
  }, [defaultImage])

  return (
    <>
      <Text padding='0 0 .5rem 0'>
        {label}
      </Text>

      <Label htmlFor={id}>
        <Input
          type='file'
          id={id}
          ref={ref}
          onChange={handleChange}
          {...restProps}
        />

        <Preview>
          {
            imagePreview &&
              <Image src={imagePreview} width='50px' margin='0 1rem 0 0' />
          }

          {
            file
              ? <PreviewText file={file} />
              : 'Selecione seu arquivo'
          }
        </Preview>
      </Label>
    </>
  )
})

InputFile.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  defaultImage: PropTypes.string,
}

export default InputFile
