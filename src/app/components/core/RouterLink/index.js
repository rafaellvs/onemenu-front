import styled, { css } from 'styled-components'
import { Link } from '@reach/router'

import theme from 'app/helpers/theme'

const RouterLink = styled(Link)`
  ${css`${theme.variants.default}`}
  ${({ variant }) => variant && css`${theme.variants[variant]}`}
  
  text-decoration: ${({ textDecoration }) => textDecoration || 'underline'};
  cursor: pointer;
  transition: color .3s;

  &:hover {
    color: ${theme.colors.primary};
  }
`

export default RouterLink
