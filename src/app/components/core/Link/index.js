import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

export const Link = styled.a`
  ${css`${theme.variants.default}`}
  ${({ variant }) => variant && css`${theme.variants[variant]}`}
  
  text-decoration: ${({ textDecoration }) => textDecoration || 'underline'};
  cursor: pointer;
  transition: color .3s;

  &:hover {
    color: ${theme.colors.primary};
  }
`

export default Link
