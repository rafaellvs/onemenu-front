import styled from 'styled-components'

import theme from 'app/helpers/theme'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Icon = styled(FontAwesomeIcon)`
  font-size: ${({ fontSize }) => fontSize || '1rem'};
  color: ${({ color }) => color || theme.colors.black};
  transition: color .3s;

  ${({ hover }) => hover &&
    `
      &:hover {
        color: ${hover};
      }
    `
  }
`
export default Icon
