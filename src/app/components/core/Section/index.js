import styled from 'styled-components'

const Section = styled.section`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  margin: 0 auto;
`

export default Section
