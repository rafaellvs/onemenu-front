import styled from 'styled-components'

const Label = styled.label`
  display: block;
  padding-bottom: .5rem;
`

export default Label
