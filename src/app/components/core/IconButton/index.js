import React from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'

import { Button, Icon } from './styled'

const IconButton = ({ icon, children, ...restProps }) =>
  <Button {...restProps}>
    <Icon icon={icon} />
    <Text component='span'>
      {children}
    </Text>
  </Button>

IconButton.propTypes = {
  icon: PropTypes.object,
  children: PropTypes.node,
}

export default IconButton
