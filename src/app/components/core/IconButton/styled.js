import styled from 'styled-components'

import theme from 'app/helpers/theme'

import CoreButton from 'app/components/core/Button'
import CoreIcon from 'app/components/core/Icon'

export const Button = styled(CoreButton)`
  position: relative;
  display: flex;
  justify-content: center;
  min-width: 250px;
  background: ${theme.colors.black};

  span {
    color: ${theme.colors.white};
  }
`

export const Icon = styled(CoreIcon)`
  position: absolute;
  left: 1rem;
  color: ${theme.colors.white};
`
