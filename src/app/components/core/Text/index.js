import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

import CoreText from './text'

const Text = styled(CoreText)`
  ${css`${theme.variants.default}`}
  ${({ variant }) => variant && css`${theme.variants[variant]}`}
  
  padding: ${({ padding }) => padding || '0'};
  font-weight: ${({ bold }) => bold && 'bold'};
  margin: 0;
  transition: color .3s;
`

export default Text
