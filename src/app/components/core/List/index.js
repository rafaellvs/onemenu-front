import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Li = styled.li`
  list-style: none;
`

const List = ({ children, className }) =>
  <ul className={className}>
    {children.map((child, index) =>
      <Li key={index}>
        {child}
      </Li>
    )}
  </ul>

List.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
}

export default List
