import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

export const StyledSelect = styled.select`
  ${css`${theme.variants.default}`}

  display: block;
  width: ${({ width }) => width || '100%'};
  min-width: ${({ minWidth }) => minWidth};
  background: ${theme.colors.white};
  padding: 1rem;
  border: solid 2px ${theme.colors.lightGray};
  border-radius: ${theme.borderRadius.default};
  margin-bottom: 1.2rem;
`
