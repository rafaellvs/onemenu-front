import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { StyledSelect } from './styled'

const Select = React.forwardRef(({ defaultValue, children, ...restProps }, ref) => {
  const [value, setValue] = useState('')

  const handleChange = event =>
    setValue(event.target.value)

  useEffect(() => {
    defaultValue && setValue(defaultValue)
  }, [defaultValue])

  return (
    <StyledSelect
      value={value}
      onChange={handleChange}
      ref={ref}
      {...restProps}
    >
      {children}
    </StyledSelect>
  )
})

Select.propTypes = {
  defaultValue: PropTypes.string,
  children: PropTypes.node,
}

export default Select
