import styled from 'styled-components'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 300px;
`

export const TabsContainer = styled.div`
  display: flex;
`

export const Tab = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
  padding: 1rem .5rem;
  border-top-right-radius: ${theme.borderRadius.box};
  border-top-left-radius: ${theme.borderRadius.box};
  background: ${({ active }) => active ? theme.colors.white : theme.colors.background};
  color: ${({ active }) => active ? theme.colors.black : theme.colors.gray};
  transition: background .3s;
  cursor: pointer;

  ${
    ({ active }) => !active && `
      &:hover {
        background: ${theme.colors.lighterGray};
        opacity: .8;
      }
    `
  }
`

export const ContentContainer = styled.div`
  background: ${theme.colors.white};
  border-bottom-right-radius: ${theme.borderRadius.box};
  border-bottom-left-radius: ${theme.borderRadius.box};
`

export const Content = styled.div`
  display: ${({ active }) => active ? 'block' : 'none'};
`
