import React, { useState } from 'react'
import PropTypes from 'prop-types'

import {
  Container,
  TabsContainer,
  Tab,
  ContentContainer,
  Content,
} from './styled'

const Tabs = ({ tabs, children }) => {
  const [selected, setSelected] = useState(0)

  return (
    <Container>
      <TabsContainer>
        {
          tabs.map((tab, index) =>
            <Tab
              key={index}
              onClick={() => setSelected(index)}
              active={index === selected}
            >
              <h3>{tab}</h3>
            </Tab>
          )
        }
      </TabsContainer>

      <ContentContainer>
        {
          children.map((child, index) =>
            <Content
              key={index}
              active={index === selected}
            >
              {child}
            </Content>
          )
        }
      </ContentContainer>
    </Container>
  )
}

Tabs.propTypes = {
  tabs: PropTypes.array,
  children: PropTypes.node,
}

export default Tabs
