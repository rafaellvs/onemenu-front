import React from 'react'
import PropTypes from 'prop-types'

import { StyledImage } from './styled'

const Image = ({ src, width, margin, className, ...restProps }) =>
  <StyledImage
    src={src}
    width={width}
    margin={margin}
    className={className}
    {...restProps}
  />

Image.propTypes = {
  src: PropTypes.string,
  width: PropTypes.string,
  margin: PropTypes.string,
  className: PropTypes.string,
}

export default Image
