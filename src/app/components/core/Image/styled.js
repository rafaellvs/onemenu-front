import styled from 'styled-components'

export const StyledImage = styled.img`
  width: ${({ width }) => width || 'auto'};
  margin: ${({ margin }) => margin || '0'};
  box-sizing: content-box;
`
