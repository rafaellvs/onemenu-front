import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

const Textarea = styled.textarea`
  ${css`${theme.variants.default}`}

  display: block;
  width: ${({ width }) => width || '100%'};
  min-width: ${({ minWidth }) => minWidth};
  padding: 1rem;
  border: solid 2px ${theme.colors.lightGray};
  border-radius: ${theme.borderRadius.default};
  margin-bottom: 1.2rem;
  resize: none;
`

export default Textarea
