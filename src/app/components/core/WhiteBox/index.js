import styled from 'styled-components'

import theme from 'app/helpers/theme'

import { mediaQueryMax } from 'app/helpers/media-queries'

const WhiteBox = styled.div`
  background: ${theme.colors.white};
  border-radius: ${theme.borderRadius.box};
  padding: 2.5rem;
  margin: ${({ margin }) => margin || 'auto'};

  ${mediaQueryMax('xSmall', `padding: ${theme.padding.small}`)}
`

export default WhiteBox
