import React from 'react'
import PropTypes from 'prop-types'

import { Container } from './styled'

const Modal = ({ isOpen, children }) =>
  <Container isOpen={isOpen}>
    {children}
  </Container>

Modal.propTypes = {
  isOpen: PropTypes.bool,
  children: PropTypes.node,
}

export default Modal
