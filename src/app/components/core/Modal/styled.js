import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

import { mediaQueryMax } from 'app/helpers/media-queries'

export const Container = styled.div`
  visibility: hidden;
  opacity: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background: rgba(90, 90, 90, 0.9);
  padding: ${theme.padding.default};
  z-index: 200;
  transition: opacity .3s, visibility .3s;

  ${mediaQueryMax('xSmall', `padding: ${theme.padding.small}`)}

  & > div {
    ${mediaQueryMax('xSmall', `width: ${theme.width.xSmall}`)}

    width: ${theme.width.default};
  }

  ${({ isOpen }) => isOpen &&
    css`
      visibility: visible;
      opacity: 1;
    `
  }
`
