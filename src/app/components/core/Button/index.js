import styled from 'styled-components'

import theme from 'app/helpers/theme'

const Button = styled.button`
  width: ${({ width }) => width || '100%'};
  min-width: ${({ minWidth }) => minWidth};
  margin: ${({ margin }) => margin};
  background: ${({ remove }) => remove ? theme.colors.error : theme.colors.primary};
  color: ${theme.colors.white};
  font-weight: bold;
  border: none;
  border-radius: ${theme.borderRadius.default};
  padding: 1rem;
  transition: background .3s;

  &:hover:not([disabled]) {
    background: ${({ remove }) => remove ? theme.colors.errorHover : theme.colors.primaryHover};
    cursor: pointer;
  }

  &:disabled {
    background: ${theme.colors.gray};
  }
`

export default Button
