import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import {
  Container,
  Input,
  Radio,
  InnerBall,
  Label,
} from './styled'

const InputRadio = ({ children, register }) => {
  const [selectedValue, setSelectedValue] = useState(false)

  const handleChange = event =>
    setSelectedValue(event.target.value)

  const handleClick = child => {
    setSelectedValue(child.props.value)
    const input = document.querySelector(`#${child.props.id}`)
    input.click()
  }

  useEffect(() => {
    children.map(child =>
      child.props.checked &&
      setSelectedValue(child.props.value)
    )
  }, [])

  return (
    children.map((child, index) =>
      <Container key={index}>
        <Input
          type='radio'
          id={child.props.id}
          name={child.props.name}
          value={child.props.value}
          ref={register}
          checked={selectedValue === child.props.value}
          onChange={handleChange}
        />

        <Radio onClick={() => handleClick(child)}>
          <InnerBall />
        </Radio>

        <Label htmlFor={child.props.id}>
          {child.props.label}
        </Label>
      </Container>
    )
  )
}

InputRadio.propTypes = {
  children: PropTypes.node,
  register: PropTypes.func,
}

export default InputRadio
