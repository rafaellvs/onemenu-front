import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

export const Label = styled.label`
  ${css`${theme.variants.light}`}

  &:hover {
    cursor: pointer;
  }
`

export const Input = styled.input`
  position: absolute;
  visibility: hidden;
  opacity: 0;
`

export const Radio = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  border: none;
  margin-right: .5rem;
  background: ${theme.colors.background};
  transition: border .3s, background .3s;

  &:hover {
    cursor: pointer;
  }
`

export const InnerBall = styled.div`
  visibility: hidden;
  opacity: 0;
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: ${theme.colors.primary};
  transition: visibility .3s, opacity .3s;
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: .5rem 0;

  & ${Input}:checked + ${Radio} {
    background: none;
    border: 1px solid ${theme.colors.primary};

    ${InnerBall} {
      visibility: visible;
      opacity: 1;
    }
  }
`
