import styled from 'styled-components'

const Box = styled.div`
  padding: ${({ padding }) => padding};
  margin: ${({ margin }) => margin};
  text-align: ${({ align }) => align};
`

export default Box
