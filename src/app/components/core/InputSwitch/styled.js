import styled from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  display: flex;
  align-items: center;
`

export const Label = styled.label`
  position: relative;
  width: 50px;
  height: 25px;
  border-radius: 20px;
  background: ${theme.colors.lightGray};
  cursor: pointer;

  &::before {
    content: '';
    position: absolute;
    left: ${({ isChecked }) => isChecked ? '25px' : 0};
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: ${({ isChecked }) => isChecked ? theme.colors.primary : theme.colors.gray};
    transition: left .3s, background .3s;

    ${mediaQueryMax('xSmall', `
      width: 20px;
      height: 20px;
    `)}
  }

  ${mediaQueryMax('xSmall', `
      width: 40px;
      height: 20px;
    `)}
`

export const Switch = styled.input`
  position: absolute;
  visibility: hidden;
`
