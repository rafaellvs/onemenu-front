import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'

import { Container, Label, Switch } from './styled'

const InputSwitch = React.forwardRef(({
  id,
  name,
  label,
  statusText,
  checked,
  setIsActive,
}, ref) => {
  const [isChecked, setIsChecked] = useState(false)

  const handleChange = () => {
    setIsChecked(!isChecked)
    setIsActive && setIsActive(!isChecked)
  }

  useEffect(() => {
    checked && setIsChecked(true)
  }, [])

  return (
    <Container>
      {label && <Text padding='0 1rem 0 0'>{label}</Text>}
      <Label
        id={id}
        onClick={handleChange}
        isChecked={isChecked}
      />

      <Switch
        type='checkbox'
        id={id}
        name={name}
        checked={isChecked}
        ref={ref}
        onChange={handleChange}
      />
      {
        statusText &&
          <Text padding='0 0 0 1rem'>
            {isChecked ? 'Ativa' : 'Inativa'}
          </Text>
      }
    </Container>
  )
})

InputSwitch.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  statusText: PropTypes.bool,
  checked: PropTypes.bool,
  setIsActive: PropTypes.func,
}

export default InputSwitch
