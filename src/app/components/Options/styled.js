import styled, { css } from 'styled-components'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  position: relative;
`

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 20px;
  padding: .2rem;
  background: ${theme.colors.lightGray};
  border-radius: 3px;
  transition: background .3s;

  svg {
    color: ${theme.colors.gray};
  }

  &:hover {
    cursor: pointer;
    background: ${theme.colors.primary};

    svg {
      color: ${theme.colors.black};
    }
  }
`

export const OptionsContainer = styled.div`
  visibility: hidden;
  opacity: 0;
  position: absolute;
  top: 0;
  z-index: 100;
  right: 30px;
  padding: 1rem;
  border-radius: ${theme.borderRadius.default};
  box-shadow: 0px 5px 5px 0px rgba(0,0,0,0.4);
  background: ${theme.colors.white};
  transition: visibility .3s, opacity .3s;

  ${({ isDialogOpen }) => isDialogOpen &&
    css`
      visibility: visible;
      opacity: 1;
    `
  }
`

export const Option = styled.div`
  min-width: 100px;
  padding: .3rem 0;

  a {
    font-size: .8rem;
    text-decoration: none;
    font-weight: bold;
  }
`
