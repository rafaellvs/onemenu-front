import React from 'react'
import PropTypes from 'prop-types'

import { useDialog } from 'app/hooks/dialog'

import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'

import Icon from 'app/components/core/Icon'

import {
  Container,
  IconContainer,
  OptionsContainer,
  Option,
} from './styled'

const Options = ({ children }) => {
  const { ref, isDialogOpen, setDialogOpen } = useDialog()

  return (
    <Container>
      <IconContainer onClick={() => setDialogOpen(true)}>
        <Icon icon={faEllipsisV} />
      </IconContainer>

      <OptionsContainer ref={ref} isDialogOpen={isDialogOpen}>
        {
          children.map((child, index) =>
            <Option key={index}>
              {child}
            </Option>
          )
        }
      </OptionsContainer>
    </Container>
  )
}

Options.propTypes = {
  children: PropTypes.node,
}

export default Options
