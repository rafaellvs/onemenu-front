import React, { useEffect } from 'react'
import { Router, useLocation, navigate } from '@reach/router'

import { getCurrentUser } from 'app/helpers/parse-api/account'

import Section from 'app/components/core/Section'

import NavBar from 'app/components/NavBar'

// Account
import Login from 'app/pages/Account/Login'
import SignUp from 'app/pages/Account/SignUp'
import ResetPassword from 'app/pages/Account/ResetPassword'

// Places
import Places from 'app/pages/Places'
import PlacesIndex from 'app/pages/Places/Index'
import PlacesDashboard from 'app/pages/Places/Dashboard'

import CreatePlace from 'app/pages/Places/Create'
import CreatePlaceIndex from 'app/pages/Places/Create/Index'
import Step1 from 'app/pages/Places/Create/Step1'

// Single Place
import Place from 'app/pages/Places/Place'
import Edit from 'app/pages/Places/Place/Edit'

// Products
import Products from 'app/pages/Products'
import ProductsDashboard from 'app/pages/Products/Dashboard'

// Product
import Product from 'app/pages/Products/Product'
import ProductIndex from 'app/pages/Products/Product/Index'
import CreateProduct from 'app/pages/Products/Product/Create'
import EditProduct from 'app/pages/Products/Product/Edit'

import { MainContent, Container } from './styled'

// TODO make 404 page
const Page404 = () => <div>404</div>

const AppRouter = () => {
  const location = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [location])

  // redirects if user is logged in
  useEffect(() => {
    if (getCurrentUser()) {
      location.pathname === '/' &&
      navigate('/places/dashboard')
    } else {
      location.pathname !== '/signup' &&
      location.pathname !== '/resetpassword' &&
      navigate('/')
    }
  }, [])

  return (
    <Section>
      <NavBar />

      <MainContent>
        <Container>
          <Router>
            <Page404 default />

            <Login path='/' />
            <SignUp path='signup' />
            <ResetPassword path='resetpassword' />

            {
              getCurrentUser() &&
                <Places path='places'>
                  <PlacesIndex path='/' />
                  <PlacesDashboard path='dashboard' />

                  <CreatePlace path='create'>
                    <CreatePlaceIndex path='/' />
                    <Step1 path='step1' />
                  </CreatePlace>

                  <Place path=':id'>
                    <Edit path='edit' />

                    <Products path='products'>
                      <ProductsDashboard path='dashboard' />
                      <CreateProduct path='create' />
                      <Product path=':id'>
                        <ProductIndex path='/' />
                        <EditProduct path='edit' />
                      </Product>
                    </Products>
                  </Place>
                </Places>
            }
          </Router>
        </Container>
      </MainContent>
    </Section>
  )
}

export default AppRouter
