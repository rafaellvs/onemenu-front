import styled from 'styled-components'

import theme from 'app/helpers/theme'

import { mediaQueryMax } from 'app/helpers/media-queries'

export const MainContent = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
  background: ${theme.colors.background};
  margin-top: ${theme.navbarHeight};
  padding: 3rem;

  ${mediaQueryMax('xSmall',
    `
      padding: ${theme.padding.small};
      & > div {
        width: 100%;
      }
    `
  )}
`

export const Container = styled.div`
  ${mediaQueryMax('xSmall', `width: ${theme.width.xSmall}`)}

  width: ${theme.width.default};
`
