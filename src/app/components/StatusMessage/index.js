import React from 'react'
import PropTypes from 'prop-types'

import { Text } from './styled'

const StatusMessage = ({ message, variant }) =>
  <Text variant={variant}>
    {message}
  </Text>

StatusMessage.propTypes = {
  message: PropTypes.string,
  variant: PropTypes.string,
}

export default StatusMessage
