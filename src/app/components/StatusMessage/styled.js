import styled from 'styled-components'

import CoreText from 'app/components/core/Text'

export const Text = styled(CoreText)`
  font-size: .8rem;
  font-weight: bold;
  padding-bottom: 1rem;
`
