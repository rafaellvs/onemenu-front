import styled, { keyframes } from 'styled-components'

import theme from 'app/helpers/theme'

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Loading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 90px;
  height: 90px;
  margin: ${({ margin }) => margin || '0 auto'};
  
  &:after {
    content: " ";
    display: block;
    width: 50px;
    height: 50px;
    margin: 8px;
    border-radius: 50%;
    border: 6px solid ${theme.colors.primary};
    border-color: ${theme.colors.primary} transparent ${theme.colors.primary} transparent;
    animation: ${rotate} 1.2s linear infinite;
  }
`

export default Loading
