import styled, { css } from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

export const Container = styled.div`
  padding: 1rem ${theme.padding.default};
  border-bottom-left-radius: ${theme.borderRadius.box};
  border-bottom-right-radius: ${theme.borderRadius.box};
  border-top: 1px solid ${theme.colors.lightGray};
  transition: background .3s;

  &:hover {
    cursor: pointer;
    background: ${theme.colors.gray};
    
    p {
      color: ${theme.colors.white};
    }
  }

  ${({ item }) => item === 'Prato' &&
    css`
      border-bottom-left-radius: unset;
      border-bottom-right-radius: unset;
      border-bottom: 3px solid ${theme.colors.background};
    `
  }

${mediaQueryMax('xSmall', `padding: ${theme.padding.xSmall}`)}
`
