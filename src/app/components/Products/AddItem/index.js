import React from 'react'
import PropTypes from 'prop-types'

import Text from 'app/components/core/Text'

import { Container } from './styled'

const AddItem = ({ item, ...restProps }) => {
  return (
    <Container
      item={item}
      {...restProps}
    >
      <Text variant='light'>
        {`+ Adicionar ${item}`}
      </Text>
    </Container>
  )
}

AddItem.propTypes = {
  item: PropTypes.string,
}

export default AddItem
