import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { useDialog } from 'app/hooks/dialog'

import { getProductCategory } from 'app/helpers/parse-api/products'

import Text from 'app/components/core/Text'
import Link from 'app/components/core/Link'

import Options from 'app/components/Options'
import Products from 'app/components/Products/Products'
import EditCategory from 'app/components/Products/Dialogs/EditCategory'
import RemoveCategory from 'app/components/Products/Dialogs/RemoveCategory'

import {
  Container,
  CategoryContainer,
  Clickable,
} from './styled'

const Category = ({ category, opened }) => {
  const [exists, setExists] = useState(true)
  const [isOpen, setIsOpen] = useState(false)

  // ref, isDialogOpen, setDialogOpen
  const edit = useDialog()
  const remove = useDialog()

  // check if category got removed
  useEffect(() => {
    getProductCategory(category.id)
      .catch(() => setExists(false))
  }, [edit.isDialogOpen, remove.isDialogOpen])

  useEffect(() => {
    opened &&
    opened === category.id &&
    setIsOpen(true)
  }, [])

  return exists &&
    <Container>
      <CategoryContainer isOpen={isOpen}>
        <Clickable onClick={() => setIsOpen(!isOpen)}>
          <Text component='h4'>
            {category.get('name')}
          </Text>
        </Clickable>

        <Options>
          <Link onClick={() => edit.setDialogOpen(true)}>
            Editar categoria
          </Link>

          <Link onClick={() => remove.setDialogOpen(true)} variant='error'>
            Excluir Categoria
          </Link>
        </Options>
      </CategoryContainer>

      {isOpen && <Products id={category.id} />}

      <RemoveCategory
        id={category.id}
        name={category.get('name')}
        ref={remove.ref}
        isOpen={remove.isDialogOpen}
        setOpen={remove.setDialogOpen}
      />
      <EditCategory
        id={category.id}
        name={category.get('name')}
        ref={edit.ref}
        isOpen={edit.isDialogOpen}
        setOpen={edit.setDialogOpen}
      />
    </Container>
}

Category.propTypes = {
  category: PropTypes.object,
  opened: PropTypes.string,
}

export default Category
