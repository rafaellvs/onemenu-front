import styled from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

export const Container = styled.div``

export const CategoryContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: ${({ isOpen }) => isOpen && theme.colors.lighterGray};
  border-bottom: 1px solid ${theme.colors.background};
  transition: background .3s;
  padding: 0 ${theme.padding.default};

  &:hover {
    cursor: pointer;
    background: ${theme.colors.lighterGray};
  }

  ${mediaQueryMax('xSmall', `padding: 0 ${theme.padding.xSmall}`)}
`

export const Clickable = styled.div`
  flex: 1;
  padding: 1rem 0;
`
