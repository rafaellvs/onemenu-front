import React, { useState, useEffect } from 'react'
import { navigate } from '@reach/router'
import PropTypes from 'prop-types'

import { getProductsByCategory } from 'app/helpers/parse-api/products'

import Text from 'app/components/core/Text'

import Product from 'app/components/Products/Product'
import AddItem from 'app/components/Products/AddItem'
import Loading from 'app/components/Loading'

import { Container, Box } from './styled'

const Products = ({ id }) => {
  const [products, setProducts] = useState(null)

  const handleClick = () =>
    navigate('create', { state: { categoryId: id } })

  useEffect(() => {
    getProductsByCategory(id)
      .then(res => setProducts(res))
  }, [])

  return (
    <Container>
      {!products && <Loading />}

      {
        products &&
        !products.length &&
          <Box>
            <Text variant='error'>Nenhum prato foi encontrado.</Text>
          </Box>
      }

      {
        products &&
        products.map((product, index) =>
          <Product
            key={index}
            product={product}
          />
        )
      }

      <AddItem item='Prato' onClick={handleClick} />
    </Container>
  )
}

Products.propTypes = {
  id: PropTypes.string,
}

export default Products
