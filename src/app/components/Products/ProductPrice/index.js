import React, { useEffect } from 'react'
import { useFieldArray } from 'react-hook-form'
import PropTypes from 'prop-types'

import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'

import { Container, AddItem } from './styled'

const ProductPrice = ({ priceSelector, control, register, ...restProps }) => {
  const { fields, append } = useFieldArray({
    control,
    name: 'customPrices',
  })

  useEffect(() => {
    append({})
  }, [])

  return (
    <Container>
      {
        priceSelector === 'custom' &&
          <>
            {fields.map((item, index) =>
              <div key={item.id}>
                <Label htmlFor='customName'>Nome da variável</Label>
                <Input
                  type='text'
                  id='customName'
                  name={`customPrices[${index}].name`}
                  placeholder='Ex.: Médio'
                  ref={register()}
                />

                <Label htmlFor='customPrice'>Preço da variável</Label>
                <Input
                  type='number'
                  id='customPrice'
                  name={`customPrices[${index}].price`}
                  placeholder='Ex.: R$ 50,00'
                  ref={register()}
                />

              </div>
            )}
            <AddItem item='variável de produto' onClick={() => append({})} />
          </>
      }

      {
        priceSelector === 'simple' &&
          <>
            <Label htmlFor='price'>Preço do prato</Label>
            <Input
              type='number'
              id='price'
              name='price'
              placeholder='Ex.: R$ 50,00'
              {...restProps}
            />
          </>
      }
    </Container>
  )
}

ProductPrice.propTypes = {
  priceSelector: PropTypes.string,
  control: PropTypes.object,
  register: PropTypes.func,
}

export default ProductPrice
