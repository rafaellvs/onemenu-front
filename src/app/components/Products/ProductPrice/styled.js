import styled from 'styled-components'

import theme from 'app/helpers/theme'

import CoreAddItem from 'app/components/Products/AddItem'

export const Container = styled.div``

export const AddItem = styled(CoreAddItem)`
  border-radius: unset;
  padding: 1rem 0;

  &:hover {
    background: none;

    p {
      color: ${theme.colors.primary};
    }
  }
`
