import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useForm } from 'react-hook-form'

import { getProductCategoryData, editProductCategory } from 'app/helpers/parse-api/products'

import Modal from 'app/components/core/Modal'
import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Select from 'app/components/core/Select'
import Button from 'app/components/core/Button'

import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

const EditCategory = React.forwardRef(({ id, isOpen, setOpen }, ref) => {
  const { handleSubmit, register } = useForm()

  const [defaultValues, setDefaultValues] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [isDataLoading, setIsDataLoading] = useState(true)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })

  const onSubmit = data => {
    setApiStatus({ error: false, success: false })
    setIsLoading(true)

    editProductCategory(id, data)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Categoria alterada com sucesso.', error: false })
      })
      .catch(() => {
        setIsLoading(false)
        setApiStatus({ error: 'Não foi possível alterar a categoria.', success: false })
      })
  }

  useEffect(() => {
    getProductCategoryData(id).then(res => {
      setDefaultValues(res)
      setIsDataLoading(false)
    })
  }, [])

  // clear status messages
  useEffect(() => {
    setApiStatus({ error: false, success: false })
  }, [isOpen])

  // closes dialog after success
  useEffect(() => {
    apiStatus.success &&
    setOpen(false)
  }, [apiStatus])

  return (
    <Modal isOpen={isOpen}>
      <WhiteBox ref={ref}>
        <Text component='h3' padding='0 0 2rem 0'>
          Editar categoria
        </Text>

        {isDataLoading && <Loading />}

        {
          !isDataLoading &&
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Label htmlFor={`name-${id}`}>Nome da categoria</Label>
              <Input
                type='text'
                id={`name-${id}`}
                name='name'
                placeholder='Ex.: Pratos principais'
                ref={register({ required: true })}
                required
                defaultValue={defaultValues && defaultValues.name}
              />

              <Label htmlFor={id}>Tipo de categoria</Label>
              <Select
                id={id}
                name='category'
                ref={register({ required: true })}
                required
                defaultValue={defaultValues && defaultValues.category}
              >
                <option value=''>Selecione</option>
                <option value='Food'>Food</option>
                <option value='Drink'>Drink</option>
              </Select>

              {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
              {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

              {
                isLoading
                  ? <Loading />
                  : <Button type='submit' margin='1rem 0 0 0'>Editar categoria</Button>
              }
            </Form>
        }
      </WhiteBox>
    </Modal>
  )
})

EditCategory.propTypes = {
  id: PropTypes.string,
  isOpen: PropTypes.bool,
  setOpen: PropTypes.func,
}

export default EditCategory
