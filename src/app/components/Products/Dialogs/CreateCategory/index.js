import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useForm } from 'react-hook-form'

import { createProductCategory } from 'app/helpers/parse-api/products'

import Modal from 'app/components/core/Modal'
import WhiteBox from 'app/components/core/WhiteBox'
import Box from 'app/components/core/Box'
import Text from 'app/components/core/Text'
import Form from 'app/components/core/Form'
import Label from 'app/components/core/Label'
import Input from 'app/components/core/Input'
import Select from 'app/components/core/Select'
import Button from 'app/components/core/Button'

import StatusMessage from 'app/components/StatusMessage'
import Loading from 'app/components/Loading'

const CreateCategory = React.forwardRef(({ id, isOpen, setOpen, selected }, ref) => {
  const { handleSubmit, register } = useForm()

  const [isLoading, setIsLoading] = useState(false)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })

  const onSubmit = data => {
    setApiStatus({ error: false, success: false })
    setIsLoading(true)

    createProductCategory(id, data)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Categoria criada com successo.', error: false })
      })
      .catch(() => {
        setIsLoading(false)
        setApiStatus({ error: 'Não foi possível criar a categoria.', success: false })
      })
  }

  useEffect(() => {
    setApiStatus({ error: false, success: false })
  }, [isOpen])

  useEffect(() => {
    apiStatus.success &&
    setOpen(false)
  }, [apiStatus])

  return (
    <Modal isOpen={isOpen}>
      <WhiteBox ref={ref}>
        <Text component='h3' padding='0 0 2rem 0'>
          Cadastrar categoria
        </Text>

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Label htmlFor='name'>Nome da categoria</Label>
          <Input
            type='text'
            id='name'
            name='name'
            placeholder='Ex.: Pratos principais'
            ref={register({ required: true })}
            required
          />

          <Box padding='0 0 1rem 0'>
            <Label htmlFor='category'>Tipo de categoria</Label>
            <Select
              id='category'
              name='category'
              defaultValue={selected || ''}
              ref={register({ required: true })}
              required
            >
              <option value=''>Selecione</option>
              <option value='Food'>Food</option>
              <option value='Drink'>Drink</option>
            </Select>
          </Box>

          {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
          {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

          {
            isLoading
              ? <Loading />
              : <Button type='submit' disabled={!!apiStatus.success}>Adicionar categoria</Button>
          }
        </Form>
      </WhiteBox>
    </Modal>
  )
})

CreateCategory.propTypes = {
  id: PropTypes.string,
  isOpen: PropTypes.bool,
  setOpen: PropTypes.func,
  selected: PropTypes.string,
}

export default CreateCategory
