import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useForm } from 'react-hook-form'

import { removeProduct } from 'app/helpers/parse-api/products'

import Form from 'app/components/core/Form'
import Modal from 'app/components/core/Modal'
import WhiteBox from 'app/components/core/WhiteBox'
import Text from 'app/components/core/Text'
import Button from 'app/components/core/Button'

import Loading from 'app/components/Loading'
import StatusMessage from 'app/components/StatusMessage'

const RemoveProduct = React.forwardRef(({ id, name, isOpen, setOpen }, ref) => {
  const { handleSubmit } = useForm()

  const [isLoading, setIsLoading] = useState(false)
  const [apiStatus, setApiStatus] = useState({ error: false, success: false })

  // used a form because button onClick was triggering re-render, making modal disappear
  const onSubmit = () => {
    setIsLoading(true)
    setApiStatus({ error: false, success: false })

    removeProduct(id)
      .then(() => {
        setIsLoading(false)
        setApiStatus({ success: 'Prato removido com sucesso.', error: false })
      })
      .catch(() => {
        setIsLoading(false)
        setApiStatus({ error: 'Não foi possível remover o prato', success: false })
      })
  }

  // clear status messages
  useEffect(() => {
    setApiStatus({ error: false, success: false })
  }, [isOpen])

  // close dialog on success
  useEffect(() => {
    apiStatus.success &&
    setOpen(false)
  }, [apiStatus])

  return (
    <Modal isOpen={isOpen}>
      <WhiteBox ref={ref}>
        <Text component='h3'>
          Excluir prato
        </Text>

        <Text variant='light' padding='2rem 0'>
          Tem certeza que deseja remover o prato <span style={{ color: 'black', fontWeight: 'bold' }}>{name}</span>?
        </Text>

        {apiStatus.error && <StatusMessage message={apiStatus.error} variant='error' />}
        {apiStatus.success && <StatusMessage message={apiStatus.success} variant='success' />}

        {isLoading && <Loading />}
        {
          !isLoading &&
            <Form onSubmit={handleSubmit(onSubmit)}>
              <Button
                type='submit'
                remove
                disabled={!!apiStatus.success}
              >
                Excluir prato
              </Button>
            </Form>
        }
      </WhiteBox>
    </Modal>
  )
})

RemoveProduct.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  isOpen: PropTypes.bool,
  setOpen: PropTypes.func,
}

export default RemoveProduct
