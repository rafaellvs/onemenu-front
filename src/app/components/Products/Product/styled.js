import styled from 'styled-components'

import { mediaQueryMax } from 'app/helpers/media-queries'

import theme from 'app/helpers/theme'

import CoreBox from 'app/components/core/Box'
import Image from 'app/components/core/Image'
import Text from 'app/components/core/Text'

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1rem ${theme.padding.default};
  border-bottom: 1px solid ${theme.colors.background};

  ${mediaQueryMax('xSmall', `padding: ${theme.padding.xSmall}`)}
`

export const Box = styled(CoreBox)`
  display: flex;
  flex: 1;
  justify-content: space-between;
  align-items: center;
  margin-right: 2rem;
  transition: opacity .3s;
  opacity: ${({ isActive }) => isActive ? 1 : 0.5};

  ${mediaQueryMax('xSmall', 'margin-right: 1rem')}
`

export const NameInfo = styled.div`
  width: 50%;
  display: flex;
  align-items: center;

  img {
    margin-right: .5rem;
  }

  p {
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }

  ${mediaQueryMax('xSmall', 'width: 35%')}
`

export const Picture = styled(Image)`
  width: 50px;
  height: 50px;
  border-radius: ${theme.borderRadius.default};

  ${mediaQueryMax('xSmall', `
    width: 30px;
    height: 30px;
  `)}
`

export const Price = styled(Text)`
  padding: .5rem;
  border-radius: ${theme.borderRadius.default};
  border: 1px solid ${theme.colors.lighterGray};

  ${mediaQueryMax('xSmall', 'padding: .3rem')}
`
