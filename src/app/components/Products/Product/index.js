import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import { useDialog } from 'app/hooks/dialog'

import { getProduct, setProductStatus } from 'app/helpers/parse-api/products'

import Text from 'app/components/core/Text'
import Link from 'app/components/core/Link'
import InputSwitch from 'app/components/core/InputSwitch'

import Options from 'app/components/Options'
import RemoveProduct from 'app/components/Products/Dialogs/RemoveProduct'

import {
  Container,
  Box,
  NameInfo,
  Picture,
  Price,
} from './styled'

const Product = ({ product }) => {
  const [exists, setExists] = useState(true)
  const [isActive, setIsActive] = useState(!!product.get('status'))

  const { ref, isDialogOpen, setDialogOpen } = useDialog()

  const handlePictureSrc = () =>
    product.get('image')
      ? product.get('image')._url
      : null

  // checks if product got removed
  useEffect(() => {
    !isDialogOpen &&
    getProduct(product.id).catch(() => setExists(false))
  }, [isDialogOpen])

  // sets product status on switch click
  useEffect(() => {
    setProductStatus(product.id, isActive)
  }, [isActive])

  return exists &&
    <Container>
      <Box isActive={isActive}>
        <NameInfo>
          <Picture src={handlePictureSrc()} />
          <Text>{product.get('name')}</Text>
        </NameInfo>

        <Price>
          {
            product.get('price')
              ? `R$ ${product.get('price').toFixed(2).replace('.', ',')}`
              : '--'
          }
        </Price>

        <InputSwitch
          type='checkbox'
          checked={!!product.get('status')}
          setIsActive={setIsActive}
        />
      </Box>

      <Options>
        <Link href={product.id}>
          Ver prato
        </Link>

        <Link href={`${product.id}/edit`}>
          Editar prato
        </Link>

        <Link onClick={() => setDialogOpen(true)} variant='error'>
          Excluir prato
        </Link>
      </Options>

      <RemoveProduct
        id={product.id}
        name={product.get('name')}
        ref={ref}
        isOpen={isDialogOpen}
        setOpen={setDialogOpen}
      />
    </Container>
}

Product.propTypes = {
  product: PropTypes.object,
}

export default Product
