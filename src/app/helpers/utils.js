import React from 'react'
import Parse from 'parse'

import { getPlaceProductCategories } from 'app/helpers/parse-api/products'

// handle inputs coming from HoursInput at Restaurant/Edit
export const handleTimeInput = event => {
  // keyCode 8 === Backspace
  // keyCode 9 === Tab
  // keyCOde 46 === Delete
  if (!/\d/.test(event.key)) {
    if (event.keyCode !== 8 && event.keyCode !== 9 && event.keyCode !== 46) {
      event.preventDefault()
    }
  }

  if (event.target.value.length === 1 && event.keyCode !== 8) {
    event.target.value += event.key + ':'
    event.keyCode !== 9 && event.preventDefault()
  }
}

// validates image files
export const validateImage = file => {
  if (!/image\/*/.test(file.type)) return 'Este campo só aceita imagens'
  if (file.size > 2000000) return 'As imagens devem ter, no máximo, 2MB.'

  return true
}

// generates options for a <select> input based on registered categories
// used on Restaurant/Edit
export const generateSelectCategories = async () => {
  const query = new Parse.Query('PlaceCategory')
  const results = await query.find()

  const options = results.map((category, index) =>
    <option key={index + 1} value={category.id}>
      {category.get('name')}
    </option>
  )

  options.unshift(
    <option key={0} value=''>
      Selecione
    </option>
  )

  return options
}

// generates options for a <select> input based on registered categories by place id
// used on Product Create
export const generateSelectProductCategories = async id => {
  const categories = await getPlaceProductCategories(id)
  const merged = [...categories.food, ...categories.drink]

  const options = merged.map((category, index) =>
    <option key={index + 1} value={category.id}>
      {category.get('name')}
    </option>
  )

  options.unshift(
    <option key={0} value=''>
      Selecione
    </option>
  )

  return options
}

// generates options for a <select> input based on registered categories by product id
// used on Product Edit
export const generateSelectProductCategoriesByProductId = async id => {
  // get place
  const query = new Parse.Query('Product')
  const product = await query.get(id)

  const placeId = product.get('place').id

  const categories = await getPlaceProductCategories(placeId)
  const merged = [...categories.food, ...categories.drink]

  const options = merged.map((category, index) =>
    <option key={index + 1} value={category.id}>
      {category.get('name')}
    </option>
  )

  options.unshift(
    <option key={0} value=''>
      Selecione
    </option>
  )

  return options
}

// check if Place is fully registered. Used on Places/Dashboard
// required fields: description, category, enviroment images, address, openingHours
export const isPlaceFullyRegistered = async place => {
  const images = await place.relation('images').query().find()

  const categoryQuery = new Parse.Query('PlaceCategoryRelation')
  categoryQuery.equalTo('place', place)
  const categoryResults = await categoryQuery.find()

  if (
    place.get('description') &&
    place.get('address') &&
    place.get('openingHours') &&
    images.length &&
    categoryResults.length
  ) return true

  return false
}
