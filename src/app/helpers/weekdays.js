const weekdays = [
  {
    id: 0,
    name: 'Segunda',
    label: 'monday',
  },
  {
    id: 1,
    name: 'Terça',
    label: 'tuesday',
  },
  {
    id: 2,
    name: 'Quarta',
    label: 'wednesday',
  },
  {
    id: 3,
    name: 'Quinta',
    label: 'thursday',
  },
  {
    id: 4,
    name: 'Sexta',
    label: 'friday',
  },
  {
    id: 5,
    name: 'Sábado',
    label: 'saturday',
  },
  {
    id: 6,
    name: 'Domingo',
    label: 'sunday',
  },
]

export default weekdays
