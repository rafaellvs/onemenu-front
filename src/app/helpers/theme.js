const colors = {
  primary: '#5CE1E6',
  primaryHover: '#1EC0C6',
  secondary: '#00B2B9',
  background: '#e5e5e5',
  black: '#1A1A1A',
  white: 'white',
  gray: '#909090',
  lightGray: '#e5e5e5',
  lighterGray: '#b9b9b9',
  warning: '#fdd03a',
  error: '#de3a41',
  errorHover: '#b71f25',
  success: '#1ee11e',
}

const variants = {
  default: {
    fontFamily: 'Roboto',
    color: colors.black,
  },
  light: {
    color: colors.gray,
    fontWeight: 'normal',
  },
  error: {
    color: colors.error,
    fontWeight: 'bold',
  },
  success: {
    color: colors.success,
    fontWight: 'bold',
  },
}

const padding = {
  default: '2.5rem',
  small: '1.5rem',
  xSmall: '1rem',
}

const navbarHeight = '70px'

const borderRadius = {
  default: '5px',
  box: '10px',
}

const width = {
  xSmall: '100%',
  default: '768px',
}

const breakpoints = {
  xSmall: 480,
  small: 768,
  medium: 1024,
  large: 1280,
  xLarge: 1600,
}

const theme = {
  colors,
  variants,
  width,
  padding,
  navbarHeight,
  borderRadius,
  breakpoints,
}
export default theme
