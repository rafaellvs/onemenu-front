import Parse from 'parse'

import weekdays from 'app/helpers/weekdays'

// get User associated Places
export const getUserPlaces = async () => {
  const user = Parse.User.current()
  return await user.relation('places').query().find()
}

// add Parse Place with relation
export const createPlace = async data => {
  const user = Parse.User.current()
  const place = new Parse.Object('Place')
  const location = new Parse.GeoPoint(0, 0)

  place.set({
    name: data.name,
    location: location,
    address: 'temp',
  })
  await place.save()

  const placeRelation = user.relation('places')
  placeRelation.add(place)

  return await user.save()
}

// edit Place
export const editPlace = async (id, data) => {
  // get place
  const placeQuery = new Parse.Query('Place')
  const place = await placeQuery.get(id)

  // logo
  if (data.logo.length) {
    const file = new Parse.File('avatar', data.logo[0])
    place.set('avatar', file)
  }

  // name
  data.name && place.set('name', data.name)

  // description
  data.description && place.set('description', data.description)

  // category
  if (data.categories[0].id) {
    // get registered categories
    const registeredCategoryQuery = new Parse.Query('PlaceCategoryRelation')
    registeredCategoryQuery.equalTo('place', place)
    const registeredCategoryResults = await registeredCategoryQuery.find()
    const registeredCategories = registeredCategoryResults.map(result => result.get('category').id)

    // checks if category isnt already registered before inserting
    data.categories.map(async category => {
      if (!registeredCategories.includes(category.id)) {
        const categoryQuery = new Parse.Query('PlaceCategory')
        const newCategory = await categoryQuery.get(category.id)

        const placeCategoryRelation = new Parse.Object('PlaceCategoryRelation')
        placeCategoryRelation.set({
          place: place,
          category: newCategory,
        })
        placeCategoryRelation.save()
      }
    })

    // checks if a category got removed
    const categoriesId = data.categories.map(category => category.id)

    registeredCategories.map(async id => {
      if (!categoriesId.includes(id)) {
        const categoryQuery = new Parse.Query('PlaceCategory')
        const categoryToRemove = await categoryQuery.get(id)

        const placeCategoryRelation = new Parse.Query('PlaceCategoryRelation')
        placeCategoryRelation.equalTo('place', place)
        placeCategoryRelation.equalTo('category', categoryToRemove)
        const relationResults = await placeCategoryRelation.find()
        const relationToRemove = relationResults[0]

        relationToRemove.destroy()
      }
    })
  }

  // environment
  // old ones to be deleted
  const oldImages = await place.relation('images').query().find()

  const environment = [data.environment1, data.environment2]
  environment.map((env, index) => {
    if (env.length) {
      const Image = new Parse.Object('Image')
      const File = new Parse.File(env[0].name, env[0])

      Image.set({ file: File })
      Image.save()
        .then(async () => {
          // copy because save is needed
          const placeCopy = await placeQuery.get(id)
          placeCopy.relation('images').add(Image)
          placeCopy.save()

          oldImages[index] && oldImages[index].destroy()
        })
    }
  })

  // address
  data.address && place.set('address', data.address)

  // opening hours
  const hours = []
  weekdays.map(weekday => {
    if (data[weekday.label].isOpen) {
      data[weekday.label].hours.map(schedule => {
        schedule.timeStart &&
        hours.push({
          weekDay: weekday.id,
          timeStart: schedule.timeStart,
          timeEnd: schedule.timeEnd,
        })
      })
    }
  })
  place.set('openingHours', hours)

  // save Place
  return await place.save()
}

// get Place registered data
export const getPlaceData = async id => {
  // get place info
  const query = new Parse.Query('Place')
  const place = await query.get(id)

  // logo
  const logoQuery = place.get('avatar')
  const logo = logoQuery ? logoQuery._url : null

  // name
  const name = place.get('name')

  // description
  const description = place.get('description')

  // categories
  const categoryQuery = new Parse.Query('PlaceCategoryRelation')
  categoryQuery.equalTo('place', place)
  const categoryResults = await categoryQuery.find()
  const categories = categoryResults.map(result => result.get('category').id)

  // environment images
  const env = []
  const imagesRelation = await place.relation('images').query().find()
  imagesRelation.map(image => {
    env.push(image.get('file')._url)
  })

  // address
  const address = place.get('address')

  // openingHours
  const openingHoursQuery = place.get('openingHours')
  const openingHours = openingHoursQuery || []

  return {
    logo,
    name,
    description,
    categories,
    env1: env[0],
    env2: env[1],
    address,
    openingHours,
  }
}
