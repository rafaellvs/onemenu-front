import Parse from 'parse'

// ProductCategories
// get ProductCategory by id
export const getProductCategory = async id => {
  const query = new Parse.Query('ProductCategory')
  return query.get(id)
}

// get ProductCategories for Place by Place id
export const getPlaceProductCategories = async id => {
  const placeQuery = new Parse.Query('Place')
  const place = await placeQuery.get(id)

  const productCategoryQuery = new Parse.Query('ProductCategory')
  productCategoryQuery.equalTo('place', place)
  const results = await productCategoryQuery.find()

  // filter by Food/Drink and ignore parents
  const food = results.filter(result =>
    result.get('parent') &&
    result.get('parent').get('name') === 'Food'
  )
  const drink = results.filter(result =>
    result.get('parent') &&
    result.get('parent').get('name') === 'Drink'
  )
  const categories = { food: food, drink: drink }

  return categories
}

// create ProductCategory
export const createProductCategory = async (id, data) => {
  const placeQuery = new Parse.Query('Place')
  const place = await placeQuery.get(id)

  let categoryParent = {}

  // check if parent exists
  const categoryParentQuery = new Parse.Query('ProductCategory')
  categoryParentQuery.equalTo('place', place)
  categoryParentQuery.equalTo('parent', null)
  categoryParentQuery.equalTo('name', data.category)
  const results = await categoryParentQuery.find()

  // if no results, create parent
  if (!results.length) {
    categoryParent = new Parse.Object('ProductCategory')
    categoryParent.set('place', place)
    categoryParent.set('active', true)
    categoryParent.set('name', data.category)

    categoryParent.save()
  } else {
    categoryParent = results[0]
  }

  // create
  const category = new Parse.Object('ProductCategory')
  category.set('place', place)
  category.set('parent', categoryParent)
  category.set('active', true)
  category.set('name', data.name)

  return category.save()
}

// get ProductCategory registered data
export const getProductCategoryData = async id => {
  // get ProductCategory
  const query = new Parse.Query('ProductCategory')
  query.equalTo('objectId', id)
  const results = await query.find()
  const product = results[0]

  // name
  const name = product.get('name')

  // category
  const category = product.get('parent').get('name')

  return { name, category }
}

// edit ProductCategory
export const editProductCategory = async (id, data) => {
  const query = new Parse.Query('ProductCategory')
  const category = await query.get(id)

  // name
  data.name && category.set('name', data.name)

  // parent (Food or Drink)
  if (data.category !== category.get('parent').get('name')) {
    // get place parent
    const place = category.get('place')

    // check for ProductCategory parent (Food, Drink)
    // check if the opposite exists
    const categoryQuery = new Parse.Query('ProductCategory')
    categoryQuery.equalTo('place', place)
    categoryQuery.equalTo('parent', null)
    const results = await categoryQuery.find()

    let parent = null

    // if it does, assigns it to parent
    results.map(result => {
      if (result.get('name') === data.category) {
        parent = result
      }
    })

    // if it doesnt, create
    if (!parent) {
      const parent = new Parse.Object('ProductCategory')
      parent.set('place', place)
      parent.set('active', true)
      parent.set('name', data.category)
      parent.save()
    }

    category.set('parent', parent)
  }

  // save
  return category.save()
}

// remove ProductCategory
export const removeProductCategory = async id => {
  const query = new Parse.Query('ProductCategory')
  const category = await query.get(id)

  // remove associated Products
  const productsQuery = new Parse.Query('Product')
  productsQuery.equalTo('category', category)
  const results = await productsQuery.find()
  results.map(result => result.destroy())

  return category.destroy()
}

// Products
// get Product by id
export const getProduct = async id => {
  const query = new Parse.Query('Product')
  return query.get(id)
}

// create Product
export const createProduct = async (id, data) => {
  const placeQuery = new Parse.Query('Place')
  const place = await placeQuery.get(id)

  const categoryQuery = new Parse.Query('ProductCategory')
  const category = await categoryQuery.get(data.category)

  const product = new Parse.Object('Product')
  product.set('place', place)
  product.set('price', parseInt(data.price))

  if (data.image.length) {
    const file = new Parse.File('product', data.image[0])
    product.set('image', file)
  }

  product.set('name', data.name)
  product.set('description', data.description)
  product.set('category', category)
  product.set('status', data.status ? 1 : 0)

  return product.save()
}

// set Product status
export const setProductStatus = async (id, isActive) => {
  const query = new Parse.Query('Product')
  const product = await query.get(id)

  if (product) {
    product.set('status', isActive ? 1 : 0)
    product.save()
  }
}

// get Products by ProductCategory id
export const getProductsByCategory = async id => {
  const categoryQuery = new Parse.Query('ProductCategory')
  const category = await categoryQuery.get(id)

  const productsQuery = new Parse.Query('Product')
  productsQuery.equalTo('category', category)
  const results = await productsQuery.find()

  return results
}

// get Product registered data
export const getProductData = async id => {
  // get product
  const query = new Parse.Query('Product')
  const product = await query.get(id)

  // image
  const imageQuery = product.get('image')
  const image = imageQuery ? imageQuery._url : null

  // name
  const name = product.get('name')

  // description
  const description = product.get('description')

  // category
  const category = product.get('category').id

  // status
  const status = !!product.get('status')

  // price
  const price = product.get('price')

  return {
    image,
    name,
    description,
    category,
    status,
    price,
  }
}

// edit Product
export const editProduct = async (id, data) => {
  const productQuery = new Parse.Query('Product')
  const product = await productQuery.get(id)

  // image
  if (data.image.length) {
    const file = new Parse.File('avatar', data.image[0])
    product.set('image', file)
  }

  // name
  data.name && product.set('name', data.name)

  // description
  data.description && product.set('description', data.description)

  // category
  if (data.category !== product.get('category').id) {
    // get new category
    const categoryQuery = new Parse.Query('ProductCategory')
    categoryQuery.equalTo('objectId', data.category)
    const results = await categoryQuery.find()
    const category = results[0]

    product.set('category', category)
  }

  // status
  product.set('status', data.status ? 1 : 0)

  // price
  data.price && product.set('price', parseInt(data.price))

  return product.save()
}

// remove Product
export const removeProduct = async id => {
  const query = new Parse.Query('Product')
  const product = await query.get(id)

  return product.destroy()
}
