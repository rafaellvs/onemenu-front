import Parse from 'parse'

// Parse User Login
export const login = async (data) =>
  await Parse.User.logIn(data.email, data.password)

// Parse User Logout
export const logout = async () =>
  await Parse.User.logOut()

// Parse get current User
export const getCurrentUser = () =>
  Parse.User.current()

// Parse User SignUp
export const signup = async data => {
  const user = new Parse.User()

  user.set({
    name: data.name,
    username: data.email,
    password: data.password,
    email: data.email,
  })

  return await user.signUp()
}
